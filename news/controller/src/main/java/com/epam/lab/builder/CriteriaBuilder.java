package com.epam.lab.builder;

import com.epam.lab.dto.SearchCriteria;
import com.epam.lab.dto.SortCriteria;

import java.util.List;

public interface CriteriaBuilder {
    List<SearchCriteria> buildSearchCriteriaList(String[] searchConditions);

    List<SortCriteria> buildSortCriteriaList(String[] sortConditions);
}
