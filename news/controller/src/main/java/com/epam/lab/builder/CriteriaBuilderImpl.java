package com.epam.lab.builder;

import com.epam.lab.dto.SearchCriteria;
import com.epam.lab.dto.SortCriteria;
import com.epam.lab.exception.IllegalConditionException;
import com.epam.lab.exception.WrongQueryParameterException;
import com.epam.lab.specification.QueryOperator;
import com.epam.lab.specification.QueryParameter;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CriteriaBuilderImpl implements CriteriaBuilder {
    private static final String SEARCH_CONDITION_REGEX = "(\\w+)(<:|>:|<|>|:)([\\w; ]+)";
    private static final String SORT_CONDITION_REGEX = "(-)?(\\w+)";

    @Override
    public List<SearchCriteria> buildSearchCriteriaList(String[] searchConditions) {
        List<SearchCriteria> searchCriteriaList = new ArrayList<>();
        if (searchConditions != null && searchConditions.length > 0) {
            Pattern pattern = Pattern.compile(SEARCH_CONDITION_REGEX);
            for (String condition : searchConditions) {
                Matcher matcher = pattern.matcher(condition);
                SearchCriteria searchCriteria = buildSearchCriteria(condition, matcher);
                searchCriteriaList.add(searchCriteria);
            }
        }
        return searchCriteriaList;
    }

    @Override
    public List<SortCriteria> buildSortCriteriaList(String[] sortConditions) {
        List<SortCriteria> sortCriteriaList = new ArrayList<>();
        if (sortConditions != null && sortConditions.length > 0) {
            Pattern pattern = Pattern.compile(SORT_CONDITION_REGEX);
            for (String condition : sortConditions) {
                Matcher matcher = pattern.matcher(condition);
                SortCriteria sortCriteria = buildSortCriteria(condition, matcher);
                sortCriteriaList.add(sortCriteria);
            }
        }
        return sortCriteriaList;
    }

    private SearchCriteria buildSearchCriteria(String condition, Matcher matcher) {
        if (matcher.find()) {
            String key = matcher.group(1);
            QueryParameter queryParameter = QueryParameter.fromString(key)
                    .orElseThrow(WrongQueryParameterException::new);
            String operator = matcher.group(2);
            QueryOperator queryOperator = QueryOperator.fromString(operator)
                    .orElseThrow(WrongQueryParameterException::new);
            String valueStr = matcher.group(3);
            return new SearchCriteria(queryParameter, queryOperator, valueStr);
        }
        throw new IllegalConditionException("search", condition, SEARCH_CONDITION_REGEX);
    }

    private SortCriteria buildSortCriteria(String condition, Matcher matcher) {
        if (matcher.find()) {
            boolean ascending = matcher.group(1) == null;
            String key = matcher.group(2);
            QueryParameter queryParameter = QueryParameter.fromString(key)
                    .orElseThrow(WrongQueryParameterException::new);
            return new SortCriteria(queryParameter, ascending);
        }
        throw new IllegalConditionException("sort", condition, SORT_CONDITION_REGEX);
    }

}
