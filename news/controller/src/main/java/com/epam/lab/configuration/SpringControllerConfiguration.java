package com.epam.lab.configuration;

import com.epam.lab.builder.CriteriaBuilder;
import com.epam.lab.builder.CriteriaBuilderImpl;
import com.epam.lab.exception.RestExceptionHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
@ComponentScan({"com.epam.lab.controller", "com.epam.lab.security"})
public class SpringControllerConfiguration {

    @Bean
    public CriteriaBuilder criteriaBuilder() {
        return new CriteriaBuilderImpl();
    }

    @Bean
    public RestExceptionHandler restExceptionHandler() {
        return new RestExceptionHandler();
    }

    @Bean
    public MethodValidationPostProcessor methodValidationPostProcessor() {
        return new MethodValidationPostProcessor();
    }
}
