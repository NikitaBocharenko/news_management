package com.epam.lab.controller;

import com.epam.lab.builder.CriteriaBuilder;
import com.epam.lab.controller.util.UriGenerator;
import com.epam.lab.dto.*;
import com.epam.lab.service.NewsService;
import com.epam.lab.specification.QueryParameter;
import com.epam.lab.validation.AddingValidation;
import com.epam.lab.validation.UpdatingValidation;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.Positive;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/api/news")
@Validated
@CrossOrigin(origins = "http://localhost:3000", exposedHeaders = {"TotalEntityCount"})
public class NewsController {
    private static final Logger LOG = Logger.getLogger(NewsController.class);

    private NewsService newsService;
    private CriteriaBuilder criteriaBuilder;

    @Autowired
    public NewsController(NewsService newsService, CriteriaBuilder criteriaBuilder) {
        this.newsService = newsService;
        this.criteriaBuilder = criteriaBuilder;
    }

    /**
     * Returns a List of <code>NewsDTO</code> as a result of searching and sorting.
     *
     * <p>The <code>search</code> argument specifies search conditions. It consists of parts, that must match the regex:
     * (\w+)((<:)|(>:)|(<)|(>)|(:))([\w; ]+), and must be divided with "," character.
     * The first group of the regex specifies name of a parameter of the search. Available parameter names listed in
     * <code>QueryParameter</code> class. The second group specifies the search operator. Character ":" means operator "=".
     * The third group specifies the search value. Date value must be passed in format of the long number value. Integer,
     * long and string values can be passed "as is" without any specific changing. The search parameter can be null or empty string.
     * In this case result will contain all stored news.</p>
     *
     * <p>The <code>sort</code> argument specifies sort conditions. It consists of parts, that must match the regex: (-)?(\w+),
     * and must be divided with "," character. The first group of the regex specifies the sort order: ascending or descending,
     * where character "-" means descending sort order. The second group specifies name of a parameter of the sort. Available
     * parameter names listed in <code>QueryParameter</code> class. The sort parameter can be null or empty string.
     * In this case result will contain news without sorting.</p>
     *
     * <p>If parameter of a search or a sort will be specified incorrectly (without matching with <code>QueryParameter</code>
     * enum), result will be an error message of the <code>ErrorMessage</code> class with appropriate description.</p>
     *
     * @param search specifies search conditions
     * @param sort   specifies sort conditions
     * @return the list of <code>NewsDTO</code> as a result of searching and sorting
     * @see QueryParameter
     * @see NewsDto
     * @see com.epam.lab.exception.ErrorMessage
     */
    @GetMapping
    public List<NewsDto> find(String[] search, String[] sort, Integer page, Integer numberPerPage, HttpServletResponse response) {
        LOG.info("GET method was invoked with search conditions : " + Arrays.toString(search) + ", and sort conditions : " + Arrays.toString(sort));
        List<SearchCriteria> searchCriteriaList = criteriaBuilder.buildSearchCriteriaList(search);
        List<SortCriteria> sortCriteriaList = criteriaBuilder.buildSortCriteriaList(sort);
        if (numberPerPage == 0) {
            numberPerPage = newsService.getQuantity(new ArrayList<>()).intValue();
        }
        long totalNewsNumber = newsService.getQuantity(searchCriteriaList);
        response.addHeader("TotalEntityCount", String.valueOf(totalNewsNumber));
        List<NewsDto> newsDtoList = newsService.get(searchCriteriaList, sortCriteriaList, page, numberPerPage);
        LOG.debug(newsDtoList);
        return newsDtoList;
    }

    /**
     * Returns a single <code>NewsDTO</code> object specified by id.
     *
     * <p>The <code>id</code> argument must specify a unique identity of a <code>News</code> entity in the database.</p>
     *
     * <p>Method returns <code>NewsDTO</code> object founded by id. If entity with such id doesn't exist in the database,
     * result will contain an error message of the <code>ErrorMessage</code> class with appropriate description.</p>
     *
     * @param id specify a unique identity of a <code>News</code> entity in the database
     * @return <code>NewsDTO</code> object founded by id
     * @see NewsDto
     * @see com.epam.lab.exception.ErrorMessage
     */
    @GetMapping(value = "/{id}")
    public NewsDto find(@Positive(message = "Id must be positive")
                        @PathVariable("id") Long id) {
        LOG.info("GET method was invoked for the resource : " + id);
        return this.newsService.get(id);
    }

    /**
     * Returns a quantity of news stored in the database.
     *
     * @return quantity of news stored in the database
     */
    @GetMapping("/count")
    public Long count(String[] search) {
        LOG.info("GET method was invoked with search conditions : " + Arrays.toString(search));
        List<SearchCriteria> searchCriteriaList = criteriaBuilder.buildSearchCriteriaList(search);
        return this.newsService.getQuantity(searchCriteriaList);
    }

    /**
     * Add new news to the database and return URI of the new resource as a header of the response.
     *
     * <p>The <code>news</code> argument specifies information about new news that should be added to the database.
     * All fields of <code>NewsDTO</code> except of <code>id</code> must be filled (not null).</p>
     *
     * <p>The <code>request</code> argument specifies request that sent to the API. It's used to build a URI of a new added
     * news.</p>
     *
     * <p>The <code>response</code> argument specifies response of the API. It stores http-header "Location" with URI
     * of a new added news.</p>
     *
     * @param news     specifies information about new news that should be added to the database
     * @param request  specifies request that sent to the API. It's used to build a URI of a new added news.
     * @param response specifies response of the API. It stores http-header "Location" with URI of a new added news.
     * @see NewsDto
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void add(@Validated(AddingValidation.class) @RequestBody NewsDto news, HttpServletRequest request, HttpServletResponse response) {
        LOG.info("POST method was invoked with DTO : " + news);
        long id = newsService.add(news);
        String uri = UriGenerator.generate(request, id);
        response.addHeader("Location", uri);
    }

    /**
     * Edit existing news in the database and return URI of edited resource as a header of the response.
     *
     * <p>The <code>news</code> argument specifies new information about news stored in the database.
     * Any field of <code>news</code> expect of <code>id</code> can be null (but at least one should be not null),
     * what means that this fields won't be updated in the database.</p>
     *
     * <p>The <code>request</code> argument specifies request that sent to the API. It's used to build a URI of a edited
     * news.</p>
     *
     * <p>The <code>response</code> argument specifies response of the API. It stores http-header "Location" with URI
     * of a edited news. If entity with such id doesn't exist in the database,
     * result will contain an error message of the <code>ErrorMessage</code> class with appropriate description.</p>
     *
     * @param news     specifies new information about news stored in the database
     * @param request  specifies request that sent to the API. It's used to build a URI of a edited news.
     * @param response specifies response of the API. It stores http-header "Location" with URI of a edited news.
     * @see NewsDto
     * @see com.epam.lab.exception.ErrorMessage
     */
    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    public void edit(@Validated(UpdatingValidation.class) @RequestBody NewsDto news, HttpServletRequest request, HttpServletResponse response) {
        LOG.info("PUT method was invoked with DTO : " + news);
        long id = news.getId();
        newsService.edit(news);
        String uri = UriGenerator.generate(request, id);
        response.addHeader("Location", uri);
    }

    /**
     * Delete existing news from the database. If entity with such id doesn't exist in the database,
     * result will contain an error message of the <code>ErrorMessage</code> class with appropriate description.
     *
     * @param id specify a unique identity of a <code>News</code> entity in the database that should be deleted
     * @see com.epam.lab.exception.ErrorMessage
     */
    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@Positive(message = "Id must be positive")
                       @PathVariable("id") Long id) {
        LOG.info("DELETE method was invoked for the resource : " + id);
        newsService.delete(id);
    }
}
