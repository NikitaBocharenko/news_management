package com.epam.lab.controller;

import com.epam.lab.builder.CriteriaBuilder;
import com.epam.lab.controller.util.UriGenerator;
import com.epam.lab.dto.*;
import com.epam.lab.service.TagService;
import com.epam.lab.specification.QueryParameter;
import com.epam.lab.validation.AddingValidation;
import com.epam.lab.validation.UpdatingValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.Positive;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/tags")
@Validated
@CrossOrigin(origins = "http://localhost:3000", exposedHeaders = {"TotalEntityCount"})
public class TagController {
    private TagService tagService;
    private CriteriaBuilder criteriaBuilder;

    @Autowired
    public TagController(TagService tagService, CriteriaBuilder criteriaBuilder) {
        this.tagService = tagService;
        this.criteriaBuilder = criteriaBuilder;
    }

    /**
     * Returns a List of <code>TagDTO</code> as a result of searching and sorting.
     *
     * <p>The <code>search</code> argument specifies search conditions. It consists of parts, that must match the regex:
     * (\w+)((<:)|(>:)|(<)|(>)|(:))([\w; ]+), and must be divided with "," character.
     * The first group of the regex specifies name of a parameter of the search. Available parameter names listed in
     * <code>QueryParameter</code> class. The second group specifies a search operator. Character ":" means operator "=".
     * The third group specifies the search value. Date value must be passed in format of the long number value. Integer,
     * long and string values can be passed "as is" without any specific changing. The search parameter can be null or empty string.
     * In this case result will contain all stored tags.</p>
     *
     * <p>The <code>sort</code> argument specifies sort conditions. It consists of parts, that must match the regex: (-)?(\w+),
     * and must be divided with "," character. The first group of the regex specifies the sort order: ascending or descending,
     * where character "-" means descending sort order. The second group specifies name of a parameter of the sort. Available
     * parameter names listed in <code>QueryParameter</code> class. The sort parameter can be null or empty string.
     * In this case result will contain tags without sorting.</p>
     *
     * <p>If parameter of a search or a sort will be specified incorrectly (without matching with <code>QueryParameter</code>
     * enum), result will be an error message of the <code>ErrorMessage</code> class with appropriate description.</p>
     *
     * @param search specifies search conditions
     * @param sort   specifies sort conditions
     * @return the list of <code>TagDTO</code> as a result of searching and sorting
     * @see QueryParameter
     * @see TagDto
     * @see com.epam.lab.exception.ErrorMessage
     */
    @GetMapping
    public List<TagDto> find(String[] search, String[] sort, Integer page, Integer numberPerPage, HttpServletResponse response) {
        List<SearchCriteria> searchCriteriaList = criteriaBuilder.buildSearchCriteriaList(search);
        List<SortCriteria> sortCriteriaList = criteriaBuilder.buildSortCriteriaList(sort);
        if (numberPerPage == 0) {
            numberPerPage = tagService.getQuantity(new ArrayList<>()).intValue();
        }
        long totalTagsNumber = tagService.getQuantity(searchCriteriaList);
        response.addHeader("TotalEntityCount", String.valueOf(totalTagsNumber));
        return tagService.get(searchCriteriaList, sortCriteriaList, page, numberPerPage);
    }

    /**
     * Returns a single <code>TagDTO</code> object specified by id.
     *
     * <p>The <code>id</code> argument must specify a unique identity of a <code>Tag</code> entity in the database.</p>
     *
     * <p>Method returns <code>TagDTO</code> object founded by id. If entity with such id doesn't exist in the database,
     * result will contain an error message of the <code>ErrorMessage</code> class with appropriate description.</p>
     *
     * @param id specify a unique identity of a <code>Tag</code> entity in the database
     * @return <code>TagDTO</code> object founded by id
     * @see TagDto
     * @see com.epam.lab.exception.ErrorMessage
     */
    @GetMapping(value = "/{id}")
    public TagDto find(@Positive(message = "Id must be positive")
                       @PathVariable("id") Long id) {
        return this.tagService.get(id);
    }

    /**
     * Add new tag to the database and return URI of new resource as a header of the response.
     *
     * <p>The <code>tag</code> argument specifies information about new tag that should be added to the database.
     * All fields of <code>TagDTO</code> except of <code>id</code> must be filled (not null).</p>
     *
     * <p>The <code>request</code> argument specifies request that sent to the API. It's used to build a URI of a new added
     * tag.</p>
     *
     * <p>The <code>response</code> argument specifies response of the API. It stores http-header "Location" with URI
     * of a new added tag.</p>
     *
     * @param tag      specifies information about new tag that should be added to the database
     * @param request  specifies request that sent to the API. It's used to build a URI of a new added tag.
     * @param response specifies response of the API. It stores http-header "Location" with URI of a new added tag.
     * @see TagDto
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void add(@Validated(AddingValidation.class) @RequestBody TagDto tag, HttpServletRequest request, HttpServletResponse response) {
        long id = tagService.add(tag);
        String uri = UriGenerator.generate(request, id);
        response.addHeader("Location", uri);
    }

    /**
     * Edit existing tag in the database and return URI of edited resource as a header of the response.
     *
     * <p>The <code>tag</code> argument specifies new information about tag stored in the database.
     * All fields of <code>TagDTO</code> must be filled (not null).</p>
     *
     * <p>The <code>request</code> argument specifies request that sent to the API. It's used to build a URI of a edited
     * tag.</p>
     *
     * <p>The <code>response</code> argument specifies response of the API. It stores http-header "Location" with URI
     * of a edited tag. If entity with such id doesn't exist in the database,
     * result will contain an error message of the <code>ErrorMessage</code> class with appropriate description.</p>
     *
     * @param tag      specifies new information about tag stored in the database
     * @param request  specifies request that sent to the API. It's used to build a URI of a edited tag.
     * @param response specifies response of the API. It stores http-header "Location" with URI of a edited tag.
     * @see TagDto
     * @see com.epam.lab.exception.ErrorMessage
     */
    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    public void edit(@Validated(UpdatingValidation.class) @RequestBody TagDto tag, HttpServletRequest request, HttpServletResponse response) {
        long id = tag.getId();
        tagService.edit(tag);
        String uri = UriGenerator.generate(request, id);
        response.addHeader("Location", uri);
    }

    /**
     * Delete existing tag from the database. If entity with such id doesn't exist in the database,
     * result will contain an error message of the <code>ErrorMessage</code> class with appropriate description.
     *
     * @param id specify a unique identity of a <code>Tag</code> entity in the database that should be deleted
     * @see com.epam.lab.exception.ErrorMessage
     */
    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@Positive(message = "Id must be positive")
                       @PathVariable("id") Long id) {
        tagService.delete(id);
    }
}
