package com.epam.lab.controller.util;

import javax.servlet.http.HttpServletRequest;

public class UriGenerator {

    private UriGenerator() {
    }

    public static String generate(HttpServletRequest request, long id) {
        return request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + request.getServletPath() + "/" + id;

    }
}
