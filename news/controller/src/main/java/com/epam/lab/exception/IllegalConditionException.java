package com.epam.lab.exception;

public class IllegalConditionException extends RuntimeException {
    private final String type;
    private final String condition;
    private final String regex;

    public IllegalConditionException(String type, String condition, String regex) {
        this.type = type;
        this.condition = condition;
        this.regex = regex;
    }

    public String getType() {
        return type;
    }

    public String getCondition() {
        return condition;
    }

    public String getRegex() {
        return regex;
    }


}
