package com.epam.lab.exception;

import com.epam.lab.exception.util.CharsetChanger;
import com.epam.lab.specification.QueryParameter;
import org.apache.log4j.Logger;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

@ControllerAdvice
public class RestExceptionHandler {
    private static final Logger log = Logger.getLogger(RestExceptionHandler.class);

    private static final String BUNDLE_NAME = "errors";
    private static final String ROOT_KEY = "exception.";
    private static final String TITLE_KEY = "title";
    private static final String MESSAGE_KEY = "message";
    private static final String ENTITY_NOT_FOUND_KEY = "entity_not_found.";
    private static final String ENTITY_NOT_FOUND_CODE = "2001";
    private static final String WRONG_QUERY_PARAMETER_KEY = "wrong_query_parameter.";
    private static final String WRONG_QUERY_PARAMETER_CODE = "2002";
    private static final String VALIDATION_KEY = "data_not_valid.";
    private static final String VALIDATION_CODE = "2003";
    private static final String INVALID_CONDITION_KEY = "invalid_condition.";
    private static final String INVALID_CONDITION_CODE = "2004";
    private static final String ENTITY_ALREADY_EXISTS_KEY = "entity_already_exists.";
    private static final String ENTITY_ALREADY_EXISTS_CODE = "2005";

    private HttpHeaders httpHeaders;

    public RestExceptionHandler() {
        httpHeaders = new HttpHeaders();
        httpHeaders.add("Content-Type", "application/json;charset=UTF-8");
    }

    @ExceptionHandler(value = EntityNotFoundException.class)
    public ResponseEntity<ErrorMessage> handleEntityNotFoundException(EntityNotFoundException exception, Locale locale, HttpServletResponse response) {
        logException(exception);
        String exceptionKey = ENTITY_NOT_FOUND_KEY + exception.getMessage() + ".";
        String title = getValueFromBundle(exceptionKey, TITLE_KEY, locale);
        String messageTemplate = getValueFromBundle(exceptionKey, MESSAGE_KEY, locale);
        String message = MessageFormat.format(messageTemplate, exception.getId());
        setResponseEncoding(response);
        return new ResponseEntity<>(new ErrorMessage(ENTITY_NOT_FOUND_CODE, title, message), httpHeaders, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = EntityAlreadyExistsException.class)
    public ResponseEntity<ErrorMessage> handleEntityAlreadyExistsException(EntityAlreadyExistsException exception, Locale locale, HttpServletResponse response) {
        logException(exception);
        String title = getValueFromBundle(ENTITY_ALREADY_EXISTS_KEY, TITLE_KEY, locale);
        String exceptionKey = ENTITY_ALREADY_EXISTS_KEY + exception.getMessage() + ".";
        String messageTemplate = getValueFromBundle(exceptionKey, MESSAGE_KEY, locale);
        String message = MessageFormat.format(messageTemplate, exception.getEntityData());
        setResponseEncoding(response);
        return new ResponseEntity<>(new ErrorMessage(ENTITY_ALREADY_EXISTS_CODE, title, message), httpHeaders, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = WrongQueryParameterException.class)
    public ResponseEntity<ErrorMessage> handleWrongQueryParameterException(WrongQueryParameterException exception, Locale locale, HttpServletResponse response) {
        logException(exception);
        String title = getValueFromBundle(WRONG_QUERY_PARAMETER_KEY, TITLE_KEY, locale);
        String messageTemplate = getValueFromBundle(WRONG_QUERY_PARAMETER_KEY, MESSAGE_KEY, locale);
        String message = MessageFormat.format(messageTemplate, Arrays.toString(QueryParameter.values()));
        setResponseEncoding(response);
        return new ResponseEntity<>(new ErrorMessage(WRONG_QUERY_PARAMETER_CODE, title, message), httpHeaders, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorMessage> handleMethodArgumentNotValidException(MethodArgumentNotValidException exception, Locale locale, HttpServletResponse response) {
        logException(exception);
        String title = getValueFromBundle(VALIDATION_KEY, TITLE_KEY, locale);
        List<String> errors = exception.getBindingResult().getFieldErrors().stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .collect(Collectors.toList());
        String message = errors.toString();
        setResponseEncoding(response);
        return new ResponseEntity<>(new ErrorMessage(VALIDATION_CODE, title, message), httpHeaders, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = ConstraintViolationException.class)
    public ResponseEntity<ErrorMessage> handleConstraintViolationException(ConstraintViolationException exception, Locale locale, HttpServletResponse response) {
        logException(exception);
        String title = getValueFromBundle(VALIDATION_KEY, TITLE_KEY, locale);
        List<String> errors = exception.getConstraintViolations().stream()
                .map(ConstraintViolation::getMessage)
                .collect(Collectors.toList());
        String message = errors.toString();
        setResponseEncoding(response);
        return new ResponseEntity<>(new ErrorMessage(VALIDATION_CODE, title, message), httpHeaders, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = IllegalConditionException.class)
    public ResponseEntity<ErrorMessage> handleIllegalConditionException(IllegalConditionException exception, Locale locale, HttpServletResponse response) {
        logException(exception);
        String title = getValueFromBundle(INVALID_CONDITION_KEY, TITLE_KEY, locale);
        String messageTemplate = getValueFromBundle(INVALID_CONDITION_KEY, MESSAGE_KEY, locale);
        String message = MessageFormat.format(messageTemplate, exception.getType(), exception.getCondition(), exception.getRegex());
        setResponseEncoding(response);
        return new ResponseEntity<>(new ErrorMessage(INVALID_CONDITION_CODE, title, message), httpHeaders, HttpStatus.BAD_REQUEST);
    }

    private String getValueFromBundle(String exceptionKey, String typeKey, Locale locale) {
        ResourceBundle bundle = ResourceBundle.getBundle(BUNDLE_NAME, locale);
        String value = bundle.getString(ROOT_KEY + exceptionKey + typeKey);
        return CharsetChanger.fromLatin1ToUtf8(value);
    }

    private void logException(Exception exception) {
        log.error(exception);
    }

    private void setResponseEncoding(HttpServletResponse response) {
        response.setCharacterEncoding("utf-8");
    }

}
