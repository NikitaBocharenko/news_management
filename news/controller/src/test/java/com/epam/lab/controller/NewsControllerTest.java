package com.epam.lab.controller;

import com.epam.lab.builder.CriteriaBuilder;
import com.epam.lab.dto.NewsDto;
import com.epam.lab.model.Author;
import com.epam.lab.model.Tag;
import com.epam.lab.service.NewsService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(JUnit4.class)
public class NewsControllerTest {
    private NewsService mockNewsService;
    private CriteriaBuilder mockCriteriaBuilder;
    private NewsController newsController;
    private List<NewsDto> expectedNewsList;
    private NewsDto expectedNews;
    private Author author;
    private Set<Tag> tags;

    @Before
    public void setUpNewsControllerTest() {
        prepareAuthor();
        prepareTags();
        prepareExpectedNews();
        prepareExpectedNewsList();
        prepareMockNewsService();
        prepareMockCriteriaBuilder();
        prepareNewsController();
    }

    @Test
    public void shouldFindNews() {
        String[] searchConditions = {"one", "two", "three"};
        String[] sortConditions = {"four", "five", "six"};
        HttpServletResponse mockResponse = mock(HttpServletResponse.class);
        List<NewsDto> actualNewsList = newsController.find(searchConditions, sortConditions, 30, 40, mockResponse);
        assertEquals(expectedNewsList, actualNewsList);
        verify(mockResponse, times(1)).addHeader("TotalEntityCount", "3");
    }

    @Test
    public void shouldFindNewsById() {
        NewsDto actualNews = newsController.find(1L);
        assertEquals(expectedNews, actualNews);
    }

    @Test
    public void shouldAddNews() {
        HttpServletRequest mockRequest = mock(HttpServletRequest.class);
        HttpServletResponse mockResponse = mock(HttpServletResponse.class);
        newsController.add(expectedNews, mockRequest, mockResponse);
        verify(mockNewsService, times(1)).add(expectedNews);
    }

    @Test
    public void shouldEditNews() {
        HttpServletRequest mockRequest = mock(HttpServletRequest.class);
        HttpServletResponse mockResponse = mock(HttpServletResponse.class);
        newsController.edit(expectedNews, mockRequest, mockResponse);
        verify(mockNewsService, times(1)).edit(expectedNews);
    }

    @Test
    public void shouldDeleteNews() {
        newsController.delete(1L);
        verify(mockNewsService, times(1)).delete(1L);
    }

    @Test
    public void shouldCountNews() {
        long actual = newsController.count(new String[0]);
        assertEquals(expectedNewsList.size(), actual);
    }

    private void prepareAuthor() {
        author = new Author(1L, "Testname", "Testsurname");
    }

    private void prepareTags() {
        Tag tag = new Tag(1L, "testtag");
        tags = new HashSet<>();
        tags.add(tag);
    }

    private void prepareExpectedNews() {
        expectedNews = new NewsDto(1L, "Title 1", "Short text 1", "Full text 1", LocalDate.now(), LocalDate.now(), author, tags);
    }

    private void prepareExpectedNewsList() {
        expectedNewsList = new ArrayList<>();
        expectedNewsList.add(expectedNews);
        expectedNewsList.add(new NewsDto(2L, "Title 2", "Short text 2", "Full text 2", LocalDate.now(), LocalDate.now(), author, tags));
        expectedNewsList.add(new NewsDto(3L, "Title 3", "Short text 3", "Full text 3", LocalDate.now(), LocalDate.now(), author, tags));
    }

    private void prepareMockNewsService() {
        mockNewsService = mock(NewsService.class);
        when(mockNewsService.get(any(List.class), any(List.class), anyInt(), anyInt())).thenReturn(expectedNewsList);
        when(mockNewsService.get(anyLong())).thenReturn(expectedNews);
        when(mockNewsService.getQuantity(any(List.class))).thenReturn((long) expectedNewsList.size());
    }

    private void prepareMockCriteriaBuilder() {
        mockCriteriaBuilder = mock(CriteriaBuilder.class);
        when(mockCriteriaBuilder.buildSearchCriteriaList(any(String[].class))).thenReturn(new ArrayList<>());
        when(mockCriteriaBuilder.buildSortCriteriaList(any(String[].class))).thenReturn(new ArrayList<>());
    }

    private void prepareNewsController() {
        newsController = new NewsController(mockNewsService, mockCriteriaBuilder);
    }
}
