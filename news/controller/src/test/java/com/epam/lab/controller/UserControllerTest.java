package com.epam.lab.controller;

import com.epam.lab.builder.CriteriaBuilder;
import com.epam.lab.dto.RoleDto;
import com.epam.lab.dto.UserDto;
import com.epam.lab.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class UserControllerTest {
    private UserService mockUserService;
    private UserController userController;
    private CriteriaBuilder mockCriteriaBuilder;
    private List<UserDto> expectedUserList;
    private UserDto expectedUser;

    @Before
    public void setUpUserControllerTest() {
        prepareExpectedUser();
        prepareExpectedUserList();
        prepareMockUserService();
        prepareMockCriteriaBuilder();
        prepareUserController();
    }

    @Test
    public void shouldFindUsers() {
        String[] searchConditions = {"one", "two", "three"};
        String[] sortConditions = {"four", "five", "six"};
        HttpServletResponse mockResponse = mock(HttpServletResponse.class);
        List<UserDto> actualUsers = userController.find(searchConditions, sortConditions, 20, 30, mockResponse);
        assertEquals(expectedUserList, actualUsers);
        verify(mockResponse, times(1)).addHeader("TotalEntityCount", "3");
    }

    @Test
    public void shouldFindUserById() {
        UserDto actualUser = userController.find(1L);
        assertEquals(expectedUser, actualUser);
    }

    @Test
    public void shouldAddUser() {
        HttpServletRequest mockRequest = mock(HttpServletRequest.class);
        HttpServletResponse mockResponse = mock(HttpServletResponse.class);
        userController.add(expectedUser, mockRequest, mockResponse);
        verify(mockUserService, times(1)).add(expectedUser);
    }

    @Test
    public void shouldEditUser() {
        HttpServletRequest mockRequest = mock(HttpServletRequest.class);
        HttpServletResponse mockResponse = mock(HttpServletResponse.class);
        userController.edit(expectedUser, mockRequest, mockResponse);
        verify(mockUserService, times(1)).edit(expectedUser);
    }

    @Test
    public void shouldDeleteUser() {
        userController.delete(1L);
        verify(mockUserService, times(1)).delete(1L);
    }

    private void prepareExpectedUser() {
        Set<RoleDto> roles = new HashSet<>();
        roles.add(new RoleDto(1L, "TestRole1"));
        expectedUser = new UserDto(1L, "TestName1", "TestSurname1", "TestLogin1", "TestPassword1",roles);
    }

    private void prepareExpectedUserList() {
        expectedUserList = new ArrayList<>();
        expectedUserList.add(expectedUser);
        Set<RoleDto> roles = new HashSet<>();
        roles.add(new RoleDto(2L, "TestRole2"));
        expectedUserList.add(new UserDto(2L, "TestName2", "TestSurname2", "TestLogin2", "TestPassword2",roles));
        roles = new HashSet<>();
        roles.add(new RoleDto(3L, "TestRole3"));
        expectedUserList.add(new UserDto(3L, "TestName3", "TestSurname3", "TestLogin3", "TestPassword3",roles));
    }

    private void prepareMockUserService() {
        mockUserService = mock(UserService.class);
        when(mockUserService.get(any(List.class), any(List.class), anyInt(), anyInt())).thenReturn(expectedUserList);
        when(mockUserService.get(anyLong())).thenReturn(expectedUser);
        when(mockUserService.getQuantity(any(List.class))).thenReturn((long) expectedUserList.size());
    }

    private void prepareMockCriteriaBuilder() {
        mockCriteriaBuilder = mock(CriteriaBuilder.class);
        when(mockCriteriaBuilder.buildSearchCriteriaList(any(String[].class))).thenReturn(new ArrayList<>());
        when(mockCriteriaBuilder.buildSortCriteriaList(any(String[].class))).thenReturn(new ArrayList<>());
    }

    private void prepareUserController() {
        userController = new UserController(mockUserService, mockCriteriaBuilder);
    }
}
