package com.epam.lab.exception;

public class EntityNotFoundException extends RuntimeException {
    private final long id;

    public EntityNotFoundException(String message, Throwable cause, long id) {
        super(message, cause);
        this.id = id;
    }

    public EntityNotFoundException(String message, long id) {
        super(message);
        this.id = id;
    }

    public long getId() {
        return id;
    }
}
