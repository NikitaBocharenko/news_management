package com.epam.lab.exception;

public class NewsNotFoundException extends EntityNotFoundException {
    public NewsNotFoundException(long id) {
        super("news", id);
    }
}
