package com.epam.lab.exception;

public class UserNotFoundException extends EntityNotFoundException {
    public UserNotFoundException(long id) {
        super("user", id);
    }
}
