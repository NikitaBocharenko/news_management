package com.epam.lab.repository;

import com.epam.lab.exception.NewsNotFoundException;
import com.epam.lab.model.News;
import com.epam.lab.specification.ConditionBuilder;
import com.epam.lab.specification.SearchSpecification;
import com.epam.lab.specification.SortSpecification;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.List;

@Repository
public class NewsRepositoryImpl implements NewsRepository {
    @PersistenceContext
    private EntityManager entityManager;

    public NewsRepositoryImpl() {
    }

    public NewsRepositoryImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void add(News entity) {
        entityManager.persist(entity);
    }

    @Override
    public void set(News entity) {
        findOne(entity.getId());
        entityManager.merge(entity);
    }

    @Override
    public void remove(Long id) {
        News entity = findOne(id);
        entityManager.remove(entity);
    }

    @Override
    public News findOne(Long id) {
        News news = entityManager.find(News.class, id);
        if (news == null) {
            throw new NewsNotFoundException(id);
        }
        return news;
    }

    @Override
    public List<News> find(List<SearchSpecification<News>> searchSpecifications, List<SortSpecification<News>> sortSpecifications, int offset, int limit) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<News> criteriaQuery = criteriaBuilder.createQuery(News.class);
        Root<News> newsRoot = criteriaQuery.from(News.class);
        Predicate[] predicates = ConditionBuilder.buildPredicates(searchSpecifications, newsRoot, criteriaBuilder);
        Order[] orders = ConditionBuilder.buildOrders(sortSpecifications, newsRoot, criteriaBuilder);
        criteriaQuery.select(newsRoot).where(predicates).orderBy(orders);
        TypedQuery<News> query = entityManager.createQuery(criteriaQuery).setFirstResult(offset).setMaxResults(limit);;
        return query.getResultList();
    }

    @Override
    public Long count(List<SearchSpecification<News>> searchSpecifications) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<News> newsRoot = criteriaQuery.from(News.class);
        Predicate[] predicates = ConditionBuilder.buildPredicates(searchSpecifications, newsRoot, criteriaBuilder);
        criteriaQuery.select(criteriaBuilder.count(newsRoot)).where(predicates);
        TypedQuery<Long> query = entityManager.createQuery(criteriaQuery);
        return query.getSingleResult();
    }
}
