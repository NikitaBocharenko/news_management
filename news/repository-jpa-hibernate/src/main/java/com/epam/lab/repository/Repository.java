package com.epam.lab.repository;

import com.epam.lab.specification.SearchSpecification;
import com.epam.lab.specification.SortSpecification;

import java.util.List;

public interface Repository<T> {
    void add(T entity);

    void set(T entity);

    void remove(Long id);

    T findOne(Long id);

    List<T> find(List<SearchSpecification<T>> searchSpecifications, List<SortSpecification<T>> sortSpecifications, int offset, int limit);

    Long count(List<SearchSpecification<T>> searchSpecifications);
}
