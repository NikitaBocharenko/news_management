package com.epam.lab.specification;

import java.util.Optional;

public enum QueryOperator {
    EQUALS, GREATER, GREATER_OR_EQUALS, LOWER, LOWER_OR_EQUALS;

    private static final String EQUALS_STR = ":";
    private static final String GREATER_STR = ">";
    private static final String GREATER_OR_EQUALS_STR = ">:";
    private static final String LOWER_STR = "<";
    private static final String LOWER_OR_EQUALS_STR = "<:";

    public static Optional<QueryOperator> fromString(String operatorStr) {
        switch (operatorStr) {
            case EQUALS_STR:
                return Optional.of(EQUALS);
            case GREATER_STR:
                return Optional.of(GREATER);
            case LOWER_STR:
                return Optional.of(LOWER);
            case GREATER_OR_EQUALS_STR:
                return Optional.of(GREATER_OR_EQUALS);
            case LOWER_OR_EQUALS_STR:
                return Optional.of(LOWER_OR_EQUALS);
            default:
                return Optional.empty();
        }
    }
}
