package com.epam.lab.specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import java.util.Objects;

public class SortSpecificationImpl<T> implements SortSpecification<T> {
    private QueryParameter queryParameter;
    private boolean ascending;

    public SortSpecificationImpl(QueryParameter queryParameter, boolean ascending) {
        this.queryParameter = queryParameter;
        this.ascending = ascending;
    }

    @Override
    public Order toOrder(Root<T> root, CriteriaBuilder criteriaBuilder) {
        return ascending ?
                criteriaBuilder.asc(root.get(queryParameter.getParameterName())) :
                criteriaBuilder.desc(root.get(queryParameter.getParameterName()));
    }

    @Override
    public String toString() {
        return "JpaSortSpecificationImpl{" +
                "queryParameter=" + queryParameter +
                ", ascending=" + ascending +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SortSpecificationImpl<?> that = (SortSpecificationImpl<?>) o;
        return ascending == that.ascending &&
                queryParameter == that.queryParameter;
    }

    @Override
    public int hashCode() {
        return Objects.hash(queryParameter, ascending);
    }
}
