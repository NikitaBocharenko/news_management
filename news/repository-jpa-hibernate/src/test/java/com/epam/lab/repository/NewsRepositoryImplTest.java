package com.epam.lab.repository;

import com.epam.lab.model.Author;
import com.epam.lab.model.News;
import com.epam.lab.model.Tag;
import com.epam.lab.specification.*;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.hibernate.jpa.HibernatePersistenceProvider;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.sql.DataSource;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.*;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class NewsRepositoryImplTest {
    private static final String RESOURCES_PATH = "src" + File.separator + "test" + File.separator + "resources" + File.separator;
    private static final String CREATE_SCHEMA_SQL_FILE_NAME = "create_schema.sql";
    private static final String DROP_SCHEMA_SQL_FILE_NAME = "drop_schema.sql";
    private static final String NEWS_DATA_FILE_NAME = "news.txt";
    private static final String AUTHOR_DATA_FILE_NAME = "author.txt";
    private static final String TAG_DATA_FILE_NAME = "tag.txt";
    private static DataSource dataSource;
    private static Properties hibernateProperties;
    private EntityManagerFactory entityManagerFactory;
    private EntityManager entityManager;
    private NewsRepository newsJpaRepository;
    private News expectedNews;
    private List<Author> authorList = new ArrayList<>();
    private List<Tag> tagList;

    @BeforeClass
    public static void setUpDataSourceAndHibernateProperties() {
        prepareDataSource();
        prepareHibernateProperties();
    }

    private static void prepareDataSource() {
        HikariConfig hikariConfig = new HikariConfig("/database.properties");
        dataSource = new HikariDataSource(hikariConfig);
    }

    private static void prepareHibernateProperties() {
        hibernateProperties = new Properties();
        hibernateProperties.put("hibernate.dialect", "org.hibernate.dialect.HSQLDialect");
        hibernateProperties.put("hibernate.connection.driver_class", "org.hsqldb.jdbcDriver");
        hibernateProperties.put("hibernate.hbm2ddl.auto", "create-drop");
    }

    @Before
    public void setUpNewsRepositoryTest() throws IOException {
        prepareSchema();
        prepareEntityManagerFactory();
        prepareEntityManager();
        prepareAuthorData();
        prepareTagData();
        prepareExpectedNews();
        prepareNewsData();
        prepareNewsRepository();
    }

    @After
    public void cleanup() throws IOException {
        entityManager.close();
        entityManagerFactory.close();
        dropSchema();
    }

    @Test
    public void shouldFindBySearchSpecification() {
        List<SearchSpecification<News>> searchSpecifications = new ArrayList<>();
        searchSpecifications.add(new NewsByAuthorStringSearchSpecification(QueryParameter.AUTHOR_NAME, "Mikita"));
        searchSpecifications.add(new NewsByTagStringSearchSpecification(QueryParameter.TAG_NAME, "politics"));
        List<News> actual = newsJpaRepository.find(searchSpecifications, new ArrayList<>(), 0, 100);
        System.out.println(actual);
        assertEquals(1, actual.size());
        assertEquals(expectedNews, actual.get(0));
    }

    @Test
    public void shouldSortBySortSpecification() {
        List<SortSpecification<News>> sortSpecifications = new ArrayList<>();
        sortSpecifications.add(new SortSpecificationImpl<>(QueryParameter.CREATION_DATE, true));
        List<News> actual = newsJpaRepository.find(new ArrayList<>(), sortSpecifications, 0, 100);
        assertEquals(expectedNews, actual.get(2));
    }

    @Test
    public void shouldGetBySpecifiedRange() {
        List<News> actual = newsJpaRepository.find(new ArrayList<>(), new ArrayList<>(), 1, 4);
        assertEquals(2, actual.size());
    }

    @Test
    public void shouldGetOne() {
        News actual = newsJpaRepository.findOne(1L);
        assertEquals(expectedNews, actual);
    }

    @Test
    public void shouldAdd() {
        News expected = new News();
        expected.setTitle("Title 4");
        expected.setShortText("Short text 4");
        expected.setFullText("Full text 4");
        expected.setCreationDate(LocalDate.now());
        expected.setModificationDate(LocalDate.now());
        newsJpaRepository.add(expected);
        assertTrue(entityManager.contains(expected));
    }

    @Test
    public void shouldUpdate() {
        expectedNews.setTitle("Updated");
        newsJpaRepository.set(expectedNews);
        News actual = entityManager.find(News.class, 1L);
        assertEquals(expectedNews, actual);
    }

    @Test
    public void shouldDelete() {
        newsJpaRepository.remove(1L);
        News actual = entityManager.find(News.class, 1L);
        assertNull(actual);
    }

    @Test
    public void shouldCountAll() {
        long actual = newsJpaRepository.count(new ArrayList<>());
        assertEquals(3, actual);
    }

    @Test
    public void shouldCountSpecified() {
        List<SearchSpecification<News>> searchSpecifications = new ArrayList<>();
        searchSpecifications.add(new NewsByTagStringSearchSpecification(QueryParameter.TAG_NAME, "medicine"));
        long actual = newsJpaRepository.count(searchSpecifications);
        assertEquals(2, actual);
    }

    private void prepareSchema() throws IOException {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        Path path = Paths.get(RESOURCES_PATH + CREATE_SCHEMA_SQL_FILE_NAME);
        List<String> queries = Files.readAllLines(path);
        jdbcTemplate.execute(queries.get(0));
    }

    private void dropSchema() throws IOException {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        Path path = Paths.get(RESOURCES_PATH + DROP_SCHEMA_SQL_FILE_NAME);
        List<String> queries = Files.readAllLines(path);
        jdbcTemplate.execute(queries.get(0));
    }

    private void prepareEntityManagerFactory() {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource);
        em.setPackagesToScan("com.epam.lab.model");
        em.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        em.setJpaProperties(hibernateProperties);
        em.setPersistenceUnitName("news_management");
        em.setPersistenceProviderClass(HibernatePersistenceProvider.class);
        em.afterPropertiesSet();
        entityManagerFactory = em.getObject();
    }

    private void prepareEntityManager() {
        entityManager = entityManagerFactory.createEntityManager();
    }

    private void prepareExpectedNews() {
        expectedNews = new News();
        expectedNews.setId(1L);
        expectedNews.setTitle("Title 1");
        expectedNews.setShortText("Short text 1");
        expectedNews.setFullText("Full text 1");
        expectedNews.setCreationDate(LocalDate.of(2020, 1, 31));
        expectedNews.setModificationDate(LocalDate.of(2020, 1, 31));
        expectedNews.setAuthor(authorList.get(0));
        Set<Tag> tagSet = new HashSet<>();
        tagSet.add(tagList.get(0));
        tagSet.add(tagList.get(1));
        tagSet.add(tagList.get(2));
        expectedNews.setTagSet(tagSet);
    }

    private void prepareNewsData() throws IOException {
        List<String> lines = readLinesFromFile(NEWS_DATA_FILE_NAME);
        EntityTransaction entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();
        int i = 0;
        for (String line : lines) {
            String[] data = line.split(",");
            News news = new News();
            news.setTitle(data[0]);
            news.setShortText(data[1]);
            news.setFullText(data[2]);
            LocalDate creationDate = LocalDate.parse(data[3]);
            news.setCreationDate(creationDate);
            LocalDate modificationDate = LocalDate.parse(data[4]);
            news.setModificationDate(modificationDate);
            news.setAuthor(authorList.get(i));
            Set<Tag> tagSet = new HashSet<>();
            tagSet.add(tagList.get(i));
            tagSet.add(tagList.get(i + 1));
            tagSet.add(tagList.get(i + 2));
            news.setTagSet(tagSet);
            entityManager.persist(news);
            i++;
        }
        entityTransaction.commit();
    }

    private void prepareAuthorData() throws IOException {
        List<String> lines = readLinesFromFile(AUTHOR_DATA_FILE_NAME);
        EntityTransaction entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();
        for (String line : lines) {
            String[] data = line.split(",");
            Author author = new Author();
            author.setName(data[0]);
            author.setSurname(data[1]);
            authorList.add(author);
            entityManager.persist(author);
        }
        entityTransaction.commit();
    }

    private void prepareTagData() throws IOException {
        tagList = new ArrayList<>();
        List<String> lines = readLinesFromFile(TAG_DATA_FILE_NAME);
        EntityTransaction entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();
        for (String line : lines) {
            Tag tag = new Tag();
            tag.setName(line);
            tagList.add(tag);
            entityManager.persist(tag);
        }
        entityTransaction.commit();
    }

    private List<String> readLinesFromFile(String filename) throws IOException {
        Path path = Paths.get(RESOURCES_PATH + filename);
        return Files.readAllLines(path);
    }

    private void prepareNewsRepository() {
        newsJpaRepository = new NewsRepositoryImpl(entityManager);
    }
}
