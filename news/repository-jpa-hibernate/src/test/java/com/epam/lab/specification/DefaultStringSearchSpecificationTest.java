package com.epam.lab.specification;

import com.epam.lab.model.News;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class DefaultStringSearchSpecificationTest {
    private Predicate mockPredicate;
    private String specifiedValue;
    private Root<News> mockRoot;
    private CriteriaBuilder mockCriteriaBuilder;
    private QueryParameter specifiedParameter;
    private Path<Object> mockPathObject;

    @Before
    public void setUpDefaultStringJpaSearchSpecificationTest() {
        prepareSpecifiedParameter();
        prepareMockPathObject();
        prepareMockRoot();
        prepareSpecifiedValue();
        prepareMockPredicate();
        prepareMockCriteriaBuilder();
    }

    @Test
    public void shouldGetPredicate() {
        DefaultStringSearchSpecification<News> specification = new DefaultStringSearchSpecification<>(specifiedParameter, specifiedValue);
        Predicate result = specification.toPredicate(mockRoot, mockCriteriaBuilder);
        assertEquals(mockPredicate, result);
    }

    private void prepareSpecifiedParameter() {
        specifiedParameter = QueryParameter.TITLE;
    }

    private void prepareMockPathObject() {
        mockPathObject = mock(Path.class);
    }

    private void prepareMockRoot() {
        mockRoot = mock(Root.class);
        when(mockRoot.get(QueryParameter.TITLE.getParameterName())).thenReturn(mockPathObject);
    }

    private void prepareSpecifiedValue() {
        specifiedValue = "value";
    }

    private void prepareMockPredicate() {
        mockPredicate = mock(Predicate.class);
    }

    private void prepareMockCriteriaBuilder() {
        mockCriteriaBuilder = mock(CriteriaBuilder.class);
        when(mockCriteriaBuilder.equal(mockPathObject, specifiedValue)).thenReturn(mockPredicate);
    }
}
