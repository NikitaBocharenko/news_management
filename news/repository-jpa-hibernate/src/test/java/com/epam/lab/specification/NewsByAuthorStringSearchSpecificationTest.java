package com.epam.lab.specification;

import com.epam.lab.model.News;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class NewsByAuthorStringSearchSpecificationTest {
    private Predicate mockPredicate;
    private String specifiedValue;
    private Root<News> mockRoot;
    private CriteriaBuilder mockCriteriaBuilder;
    private QueryParameter specifiedParameter;
    private Path<Object> mockEntityPathObject;
    private Path<Object> mockFieldPathObject;

    @Before
    public void setUpNewsByAuthorStringJpaSearchSpecificationTest() {
        prepareSpecifiedParameter();
        prepareMockFieldPathObject();
        prepareMockEntityPathObject();
        prepareMockRoot();
        prepareSpecifiedValue();
        prepareMockPredicate();
        prepareMockCriteriaBuilder();
    }

    @Test
    public void shouldGetPredicate() {
        NewsByAuthorStringSearchSpecification specification = new NewsByAuthorStringSearchSpecification(specifiedParameter, specifiedValue);
        Predicate result = specification.toPredicate(mockRoot, mockCriteriaBuilder);
        assertEquals(mockPredicate, result);
    }

    private void prepareSpecifiedParameter() {
        specifiedParameter = QueryParameter.AUTHOR_NAME;
    }

    private void prepareMockFieldPathObject() {
        mockFieldPathObject = mock(Path.class);
    }

    private void prepareMockEntityPathObject() {
        mockEntityPathObject = mock(Path.class);
        String fieldName = specifiedParameter.getParameterName();
        when(mockEntityPathObject.get(fieldName)).thenReturn(mockFieldPathObject);
    }

    private void prepareMockRoot() {
        mockRoot = mock(Root.class);
        String entityName = specifiedParameter.getEntityName();
        when(mockRoot.get(entityName)).thenReturn(mockEntityPathObject);
    }

    private void prepareSpecifiedValue() {
        specifiedValue = "value";
    }

    private void prepareMockPredicate() {
        mockPredicate = mock(Predicate.class);
    }

    private void prepareMockCriteriaBuilder() {
        mockCriteriaBuilder = mock(CriteriaBuilder.class);
        when(mockCriteriaBuilder.equal(mockFieldPathObject, specifiedValue)).thenReturn(mockPredicate);
    }
}
