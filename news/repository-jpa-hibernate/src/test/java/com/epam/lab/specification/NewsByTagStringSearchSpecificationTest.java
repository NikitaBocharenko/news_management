package com.epam.lab.specification;

import com.epam.lab.model.News;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import javax.persistence.criteria.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class NewsByTagStringSearchSpecificationTest {
    private Predicate mockPredicate;
    private String specifiedValue;
    private Root<News> mockRoot;
    private CriteriaBuilder mockCriteriaBuilder;
    private QueryParameter specifiedParameter;
    private Join<Object, Object> mockEntityJoinObject;
    private Path<Object> mockFieldPathObject;

    @Before
    public void setUpNewsByTagStringJpaSearchSpecificationTest() {
        prepareSpecifiedParameter();
        prepareMockFieldPathObject();
        prepareMockEntityJoinObject();
        prepareMockRoot();
        prepareSpecifiedValue();
        prepareMockPredicate();
        prepareMockCriteriaBuilder();
    }

    @Test
    public void shouldGetPredicate() {
        NewsByTagStringSearchSpecification specification = new NewsByTagStringSearchSpecification(specifiedParameter, specifiedValue);
        Predicate result = specification.toPredicate(mockRoot, mockCriteriaBuilder);
        assertEquals(mockPredicate, result);
    }

    private void prepareSpecifiedParameter() {
        specifiedParameter = QueryParameter.TAG_NAME;
    }

    private void prepareMockFieldPathObject() {
        mockFieldPathObject = mock(Path.class);
    }

    private void prepareMockEntityJoinObject() {
        mockEntityJoinObject = mock(Join.class);
        String fieldName = specifiedParameter.getParameterName();
        when(mockEntityJoinObject.get(fieldName)).thenReturn(mockFieldPathObject);
    }

    private void prepareMockRoot() {
        mockRoot = mock(Root.class);
        String entityName = specifiedParameter.getEntityName();
        when(mockRoot.join(entityName)).thenReturn(mockEntityJoinObject);
    }

    private void prepareSpecifiedValue() {
        specifiedValue = "value";
    }

    private void prepareMockPredicate() {
        mockPredicate = mock(Predicate.class);
    }

    private void prepareMockCriteriaBuilder() {
        mockCriteriaBuilder = mock(CriteriaBuilder.class);
        when(mockCriteriaBuilder.equal(mockFieldPathObject, specifiedValue)).thenReturn(mockPredicate);
    }
}
