package com.epam.lab.specification;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class QueryParameterTest {
    private static final String AUTHOR_NAME_PARAMETER_NAME = "name";
    private static final String AUTHOR_SURNAME_PARAMETER_NAME = "surname";
    private static final String TAG_NAME_PARAMETER_NAME = AUTHOR_NAME_PARAMETER_NAME;
    private static final String TITLE_PARAMETER_NAME = "title";
    private static final String SHORT_TEXT_PARAMETER_NAME = "shortText";
    private static final String FULL_TEXT_PARAMETER_NAME = "fullText";
    private static final String CREATION_DATE_PARAMETER_NAME = "creationDate";
    private static final String MODIFICATION_DATE_PARAMETER_NAME = "modificationDate";

    @Test
    public void shouldReturnAuthorNameParameterName() {
        String actual = QueryParameter.AUTHOR_NAME.getParameterName();
        assertEquals(AUTHOR_NAME_PARAMETER_NAME, actual);
    }

    @Test
    public void shouldReturnAuthorSurnameParameterName() {
        String actual = QueryParameter.AUTHOR_SURNAME.getParameterName();
        assertEquals(AUTHOR_SURNAME_PARAMETER_NAME, actual);
    }

    @Test
    public void shouldReturnTagNameParameterName() {
        String actual = QueryParameter.TAG_NAME.getParameterName();
        assertEquals(TAG_NAME_PARAMETER_NAME, actual);
    }

    @Test
    public void shouldReturnTitleParameterName() {
        String actual = QueryParameter.TITLE.getParameterName();
        assertEquals(TITLE_PARAMETER_NAME, actual);
    }

    @Test
    public void shouldReturnShortTextParameterName() {
        String actual = QueryParameter.SHORT_TEXT.getParameterName();
        assertEquals(SHORT_TEXT_PARAMETER_NAME, actual);
    }

    @Test
    public void shouldReturnFullTextParameterName() {
        String actual = QueryParameter.FULL_TEXT.getParameterName();
        assertEquals(FULL_TEXT_PARAMETER_NAME, actual);
    }

    @Test
    public void shouldReturnCreationDateParameterName() {
        String actual = QueryParameter.CREATION_DATE.getParameterName();
        assertEquals(CREATION_DATE_PARAMETER_NAME, actual);
    }

    @Test
    public void shouldReturnModificationDateParameterName() {
        String actual = QueryParameter.MODIFICATION_DATE.getParameterName();
        assertEquals(MODIFICATION_DATE_PARAMETER_NAME, actual);
    }

    @Test
    public void shouldCreateFromString() {
        QueryParameter queryParameter = QueryParameter.fromString("title")
                .orElseThrow(IllegalArgumentException::new);
        assertEquals(QueryParameter.TITLE, queryParameter);
    }
}
