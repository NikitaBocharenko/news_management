package com.epam.lab.builder;

import com.epam.lab.dto.SearchCriteria;
import com.epam.lab.specification.DefaultDateSearchSpecification;
import com.epam.lab.specification.QueryParameter;
import com.epam.lab.specification.SearchSpecification;

import java.time.LocalDate;

public class DefaultDateSearchSpecificationBuilder<T> implements SearchSpecificationBuilder<T> {
    @Override
    public SearchSpecification<T> build(SearchCriteria searchCriteria) {
        QueryParameter queryParameter = searchCriteria.getQueryParameter();
        String dateStr = searchCriteria.getValue();
        LocalDate date = LocalDate.parse(dateStr);
        return new DefaultDateSearchSpecification<>(queryParameter, date);
    }
}
