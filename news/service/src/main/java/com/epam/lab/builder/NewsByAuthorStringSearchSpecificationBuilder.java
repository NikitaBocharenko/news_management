package com.epam.lab.builder;

import com.epam.lab.dto.SearchCriteria;
import com.epam.lab.model.News;
import com.epam.lab.specification.NewsByAuthorStringSearchSpecification;
import com.epam.lab.specification.QueryParameter;
import com.epam.lab.specification.SearchSpecification;

public class NewsByAuthorStringSearchSpecificationBuilder implements SearchSpecificationBuilder<News> {
    @Override
    public SearchSpecification<News> build(SearchCriteria searchCriteria) {
        QueryParameter queryParameter = searchCriteria.getQueryParameter();
        String value = searchCriteria.getValue();
        return new NewsByAuthorStringSearchSpecification(queryParameter, value);
    }
}
