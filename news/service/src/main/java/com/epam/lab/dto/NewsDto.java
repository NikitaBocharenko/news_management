package com.epam.lab.dto;

import com.epam.lab.model.Author;
import com.epam.lab.model.News;
import com.epam.lab.model.Tag;
import com.epam.lab.validation.AddingValidation;
import com.epam.lab.validation.UpdatingValidation;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.validation.constraints.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class NewsDto {
    @Null(message = "Id must be absent", groups = AddingValidation.class)
    @NotNull(message = "Id must be not null", groups = UpdatingValidation.class)
    @Positive(message = "Id must be positive", groups = UpdatingValidation.class)
    private Long id;

    @NotBlank(message = "News title must be not empty", groups = {AddingValidation.class, UpdatingValidation.class})
    @Size(max = 30, message = "News title must be no larger than 30 characters", groups = {AddingValidation.class, UpdatingValidation.class})
    private String title;

    @NotBlank(message = "News short text must be not empty", groups = {AddingValidation.class, UpdatingValidation.class})
    @Size(max = 100, message = "News short text must be no larger than 100 characters", groups = {AddingValidation.class, UpdatingValidation.class})
    private String shortText;

    @NotBlank(message = "News full text must be not empty", groups = {AddingValidation.class, UpdatingValidation.class})
    @Size(max = 2000, message = "News full text must be no larger than 2000 characters", groups = {AddingValidation.class, UpdatingValidation.class})
    private String fullText;

    @NotNull(message = "News creation date must be not null", groups = {AddingValidation.class, UpdatingValidation.class})
    @PastOrPresent(message = "News creation date must be past or present", groups = {AddingValidation.class, UpdatingValidation.class})
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate creationDate;

    @NotNull(message = "News modification date must be not null", groups = {AddingValidation.class, UpdatingValidation.class})
    @PastOrPresent(message = "News modification date must be past or present", groups = {AddingValidation.class, UpdatingValidation.class})
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate modificationDate;

    @NotNull(message = "News author must be not null", groups = {AddingValidation.class, UpdatingValidation.class})
    private AuthorDto authorDto;

    @NotNull(message = "News tags must be not null", groups = {AddingValidation.class, UpdatingValidation.class})
    private Set<TagDto> tagDtoSet;

    public NewsDto() {
    }

    public NewsDto(Long id, String title, String shortText, String fullText, LocalDate creationDate, LocalDate modificationDate, Author author, Set<Tag> tags) {
        this.setId(id);
        this.setTitle(title);
        this.setShortText(shortText);
        this.setFullText(fullText);
        this.setCreationDate(creationDate);
        this.setModificationDate(modificationDate);
        this.setAuthorDto(new AuthorDto(author));
        this.setTagDtoSet(new HashSet<>());
        tags.forEach(tag -> tagDtoSet.add(new TagDto(tag)));
    }

    public NewsDto(News news, Author author, Set<Tag> tags) {
        this.setId(news.getId());
        this.setTitle(news.getTitle());
        this.setShortText(news.getShortText());
        this.setFullText(news.getFullText());
        this.setCreationDate(news.getCreationDate());
        this.setModificationDate(news.getModificationDate());
        this.setAuthorDto(new AuthorDto(author));
        this.setTagDtoSet(new HashSet<>());
        tags.forEach(tag -> tagDtoSet.add(new TagDto(tag)));
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortText() {
        return shortText;
    }

    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    public String getFullText() {
        return fullText;
    }

    public void setFullText(String fullText) {
        this.fullText = fullText;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDate getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(LocalDate modificationDate) {
        this.modificationDate = modificationDate;
    }

    public AuthorDto getAuthorDto() {
        return authorDto;
    }

    public void setAuthorDto(AuthorDto authorDto) {
        this.authorDto = authorDto;
    }

    public Set<TagDto> getTagDtoSet() {
        return new HashSet<>(tagDtoSet);
    }

    public void setTagDtoSet(Set<TagDto> tagDtoSet) {
        this.tagDtoSet = new HashSet<>(tagDtoSet);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NewsDto newsDTO = (NewsDto) o;
        return Objects.equals(id, newsDTO.id) &&
                Objects.equals(title, newsDTO.title) &&
                Objects.equals(shortText, newsDTO.shortText) &&
                Objects.equals(fullText, newsDTO.fullText) &&
                Objects.equals(creationDate, newsDTO.creationDate) &&
                Objects.equals(modificationDate, newsDTO.modificationDate) &&
                Objects.equals(authorDto, newsDTO.authorDto) &&
                Objects.equals(tagDtoSet, newsDTO.tagDtoSet);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, shortText, fullText, creationDate, modificationDate, authorDto, tagDtoSet);
    }

    @Override
    public String toString() {
        return "NewsDTO{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", shortText='" + shortText + '\'' +
                ", fullText='" + fullText + '\'' +
                ", creationDate=" + creationDate +
                ", modificationDate=" + modificationDate +
                ", authorDTO=" + authorDto +
                ", tagDTOSet=" + tagDtoSet +
                '}';
    }
}
