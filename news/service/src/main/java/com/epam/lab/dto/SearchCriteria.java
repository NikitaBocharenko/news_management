package com.epam.lab.dto;

import com.epam.lab.specification.QueryOperator;
import com.epam.lab.specification.QueryParameter;

import java.util.Objects;

public class SearchCriteria {
    private QueryParameter queryParameter;
    private QueryOperator queryOperator;
    private String value;

    public SearchCriteria(QueryParameter queryParameter, QueryOperator queryOperator, String value) {
        this.queryParameter = queryParameter;
        this.queryOperator = queryOperator;
        this.value = value;
    }

    public QueryParameter getQueryParameter() {
        return queryParameter;
    }

    public QueryOperator getQueryOperator() {
        return queryOperator;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "SearchCriteria{" +
                "queryParameter=" + queryParameter +
                ", queryOperator=" + queryOperator +
                ", value='" + value + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SearchCriteria that = (SearchCriteria) o;
        return queryParameter == that.queryParameter &&
                queryOperator == that.queryOperator &&
                Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(queryParameter, queryOperator, value);
    }
}
