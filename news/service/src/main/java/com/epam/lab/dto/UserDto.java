package com.epam.lab.dto;

import com.epam.lab.model.User;
import com.epam.lab.validation.AddingValidation;
import com.epam.lab.validation.UpdatingValidation;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.security.core.userdetails.UserDetails;

import javax.validation.constraints.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class UserDto implements UserDetails {
    @Null(message = "Id must be absent", groups = AddingValidation.class)
    @NotNull(message = "Id must be not null", groups = UpdatingValidation.class)
    @Positive(message = "Id must be positive", groups = UpdatingValidation.class)
    private Long id;

    @NotBlank(message = "User name must be not empty", groups = {AddingValidation.class, UpdatingValidation.class})
    @Size(max = 30, message = "User name must be no larger than 20 characters", groups = {AddingValidation.class, UpdatingValidation.class})
    private String name;

    @NotBlank(message = "User surname must be not empty", groups = {AddingValidation.class, UpdatingValidation.class})
    @Size(max = 30, message = "User surname must be no larger than 20 characters", groups = {AddingValidation.class, UpdatingValidation.class})
    private String surname;

    @NotBlank(message = "User login must be not empty", groups = {AddingValidation.class, UpdatingValidation.class})
    @Size(max = 30, message = "User login must be no larger than 30 characters", groups = {AddingValidation.class, UpdatingValidation.class})
    private String username;

    @NotBlank(message = "User password must be not empty", groups = {AddingValidation.class})
    @Size(max = 30, message = "User password must be no larger than 30 characters", groups = {AddingValidation.class, UpdatingValidation.class})
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    @NotNull(message = "User roles must be not null", groups = {AddingValidation.class, UpdatingValidation.class})
    @Size(min = 1, message = "User must have at least 1 role", groups = {AddingValidation.class, UpdatingValidation.class})
    private Set<RoleDto> authorities;

    public UserDto() {
    }

    public UserDto(Long id, String name, String surname, String username, String password, Set<RoleDto> authorities) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.username = username;
        this.password = password;
        this.authorities = authorities;
    }

    public UserDto(User user) {
        this.id = user.getId();
        this.name = user.getName();
        this.surname = user.getSurname();
        this.username = user.getLogin();
        this.password = user.getPassword();
        this.authorities = new HashSet<>();
        user.getRoleSet().forEach(role -> {
            RoleDto roleDto = new RoleDto(role);
            this.authorities.add(roleDto);
        });
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public Set<RoleDto> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Set<RoleDto> authorities) {
        this.authorities = authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserDto userDto = (UserDto) o;
        return Objects.equals(id, userDto.id) &&
                Objects.equals(name, userDto.name) &&
                Objects.equals(surname, userDto.surname) &&
                Objects.equals(username, userDto.username) &&
                Objects.equals(password, userDto.password) &&
                Objects.equals(authorities, userDto.authorities);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, surname, username, password, authorities);
    }

    @Override
    public String toString() {
        return "UserDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", authorities=" + authorities +
                '}';
    }
}
