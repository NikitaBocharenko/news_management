package com.epam.lab.exception;

public class AuthorAlreadyExistsException extends EntityAlreadyExistsException {
    public AuthorAlreadyExistsException(String entityData) {
        super("author", entityData);
    }
}
