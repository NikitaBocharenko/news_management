package com.epam.lab.exception;

public class TagAlreadyExistsException extends EntityAlreadyExistsException {
    public TagAlreadyExistsException(String entityData) {
        super("tag", entityData);
    }
}
