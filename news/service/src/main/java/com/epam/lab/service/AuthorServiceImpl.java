package com.epam.lab.service;

import com.epam.lab.builder.SearchSpecificationBuilderProducer;
import com.epam.lab.builder.SpecificationBuilder;
import com.epam.lab.dto.AuthorDto;
import com.epam.lab.dto.SearchCriteria;
import com.epam.lab.dto.SortCriteria;
import com.epam.lab.exception.AuthorAlreadyExistsException;
import com.epam.lab.model.Author;
import com.epam.lab.repository.AuthorRepository;
import com.epam.lab.specification.SearchSpecification;
import com.epam.lab.specification.SortSpecification;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class AuthorServiceImpl implements AuthorService {
    private final AuthorRepository authorRepository;
    private final SearchSpecificationBuilderProducer<Author> builderProducer;
    private final ModelMapper modelMapper;

    public AuthorServiceImpl(AuthorRepository authorRepository, SearchSpecificationBuilderProducer<Author> builderProducer, ModelMapper modelMapper) {
        this.authorRepository = authorRepository;
        this.builderProducer = builderProducer;
        this.modelMapper = modelMapper;
    }

    @Override
    public List<AuthorDto> get(List<SearchCriteria> searchCriteriaList, List<SortCriteria> sortCriteriaList, int page, int numberPerPage) {
        List<SearchSpecification<Author>> searchSpecifications = SpecificationBuilder.buildSearchSpecifications(searchCriteriaList, builderProducer);
        List<SortSpecification<Author>> sortSpecifications = SpecificationBuilder.buildSortSpecifications(sortCriteriaList);
        int offset = page * numberPerPage;
        long totalNumber = authorRepository.count(searchSpecifications);
        if (offset >= totalNumber) {
            offset = (int) (totalNumber - 1) / numberPerPage * numberPerPage;
        }
        List<Author> authors = authorRepository.find(searchSpecifications, sortSpecifications, offset, numberPerPage);
        return convertToDtoList(authors);
    }

    private List<AuthorDto> convertToDtoList(List<Author> authors) {
        List<AuthorDto> authorDtoList = new ArrayList<>();
        authors.forEach(author -> authorDtoList.add(modelMapper.map(author, AuthorDto.class)));
        return authorDtoList;
    }

    @Override
    public Long add(AuthorDto authorDTO) {
        Author author = modelMapper.map(authorDTO, Author.class);
        if (authorRepository.isAuthorAlreadyPersisted(author)) {
            throw new AuthorAlreadyExistsException(author.toString());
        }
        authorRepository.add(author);
        return author.getId();
    }

    @Override
    public void edit(AuthorDto authorDTO) {
        Author author = modelMapper.map(authorDTO, Author.class);
        authorRepository.set(author);
    }

    @Override
    public void delete(Long authorId) {
        authorRepository.remove(authorId);
    }

    @Override
    public AuthorDto get(Long id) {
        Author author = authorRepository.findOne(id);
        return modelMapper.map(author, AuthorDto.class);
    }

    @Override
    public Long getQuantity(List<SearchCriteria> searchCriteriaList) {
        List<SearchSpecification<Author>> searchSpecifications = SpecificationBuilder.buildSearchSpecifications(searchCriteriaList, builderProducer);
        return authorRepository.count(searchSpecifications);
    }
}
