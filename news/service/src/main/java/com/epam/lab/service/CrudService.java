package com.epam.lab.service;

import com.epam.lab.dto.SearchCriteria;
import com.epam.lab.dto.SortCriteria;

import java.util.List;

public interface CrudService<T> {
    Long add(T dto);

    void edit(T dto);

    void delete(Long id);

    T get(Long id);

    List<T> get(List<SearchCriteria> searchCriteriaList, List<SortCriteria> sortCriteriaList, int page, int numberPerPage);

    Long getQuantity(List<SearchCriteria> searchCriteriaList);
}
