package com.epam.lab.service;

import com.epam.lab.builder.SearchSpecificationBuilderProducer;
import com.epam.lab.builder.SpecificationBuilder;
import com.epam.lab.dto.*;
import com.epam.lab.exception.AuthorAlreadyExistsException;
import com.epam.lab.exception.TagAlreadyExistsException;
import com.epam.lab.model.Author;
import com.epam.lab.model.News;
import com.epam.lab.model.Tag;
import com.epam.lab.repository.AuthorRepository;
import com.epam.lab.repository.NewsRepository;
import com.epam.lab.repository.TagRepository;
import com.epam.lab.specification.SearchSpecification;
import com.epam.lab.specification.SortSpecification;
import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;

@Service
@Transactional
public class NewsServiceImpl implements NewsService {
    private static final Logger log = Logger.getLogger(NewsServiceImpl.class);

    private final NewsRepository newsRepository;
    private final AuthorRepository authorRepository;
    private final TagRepository tagRepository;
    private final SearchSpecificationBuilderProducer<News> builderProducer;
    private final ModelMapper modelMapper;

    public NewsServiceImpl(NewsRepository newsRepository, AuthorRepository authorRepository, TagRepository tagRepository,
                           SearchSpecificationBuilderProducer<News> builderProducer, ModelMapper modelMapper) {
        this.newsRepository = newsRepository;
        this.authorRepository = authorRepository;
        this.tagRepository = tagRepository;
        this.builderProducer = builderProducer;
        this.modelMapper = modelMapper;
    }

    private void addAuthorIfNecessary(AuthorDto authorDTO) {
        if (authorDTO.getId() == null) {
            log.debug("Author: " + authorDTO + " is new");
            Author author = modelMapper.map(authorDTO, Author.class);
            if (authorRepository.isAuthorAlreadyPersisted(author)) {
                throw new AuthorAlreadyExistsException(author.toString());
            }
            authorRepository.add(author);
            log.debug("Author added. Id = " + author.getId());
            authorDTO.setId(author.getId());
        } else {
            authorRepository.findOne(authorDTO.getId());
        }
    }

    private void addTagsIfNecessary(Set<TagDto> tagDtoSet) {
        if (tagDtoSet != null) {
            tagDtoSet.forEach(tagDTO -> {
                if (tagDTO.getId() == null) {
                    log.debug("Tag: " + tagDTO + " is new");
                    Tag tag = modelMapper.map(tagDTO, Tag.class);
                    if (tagRepository.isTagAlreadyPersisted(tag)) {
                        throw new TagAlreadyExistsException(tag.toString());
                    }
                    tagRepository.add(tag);
                    log.debug("Tag added. Id = " + tag.getId());
                    tagDTO.setId(tag.getId());
                } else {
                    tagRepository.findOne(tagDTO.getId());
                }
            });
        }
    }

    @Override
    public Long add(NewsDto newsDTO) {
        log.debug("Adding News");
        addAuthorIfNecessary(newsDTO.getAuthorDto());
        addTagsIfNecessary(newsDTO.getTagDtoSet());
        News news = modelMapper.map(newsDTO, News.class);
        log.debug("News is going to be added: " + news);
        newsRepository.add(news);
        return news.getId();
    }

    @Override
    public void edit(NewsDto newsDTO) {
        log.debug("Editing News");
        addAuthorIfNecessary(newsDTO.getAuthorDto());
        addTagsIfNecessary(newsDTO.getTagDtoSet());
        News news = modelMapper.map(newsDTO, News.class);
        log.debug("News is going to be edited: " + news);
        newsRepository.set(news);
    }

    @Override
    public void delete(Long newsId) {
        newsRepository.remove(newsId);
    }

    @Override
    public NewsDto get(Long id) {
        News news = newsRepository.findOne(id);
        return modelMapper.map(news, NewsDto.class);
    }

    @Override
    public List<NewsDto> get(List<SearchCriteria> searchCriteriaList, List<SortCriteria> sortCriteriaList, int page, int numberPerPage) {
        List<SearchSpecification<News>> searchSpecifications = SpecificationBuilder.buildSearchSpecifications(searchCriteriaList, builderProducer);
        List<SortSpecification<News>> sortSpecifications = SpecificationBuilder.buildSortSpecifications(sortCriteriaList);
        int offset = page * numberPerPage;
        long totalNumber = newsRepository.count(searchSpecifications);
        if (offset >= totalNumber) {
            offset = (int) (totalNumber - 1) / numberPerPage * numberPerPage;
        }
        List<News> news = newsRepository.find(searchSpecifications, sortSpecifications, offset, numberPerPage);
        return convertToDtoList(news);
    }

    private List<NewsDto> convertToDtoList(List<News> newsList) {
        List<NewsDto> newsDtoList = new ArrayList<>();
        newsList.forEach(news -> newsDtoList.add(modelMapper.map(news, NewsDto.class)));
        return newsDtoList;
    }

    @Override
    public Long getQuantity(List<SearchCriteria> searchCriteriaList) {
        List<SearchSpecification<News>> searchSpecifications = SpecificationBuilder.buildSearchSpecifications(searchCriteriaList, builderProducer);
        return newsRepository.count(searchSpecifications);
    }
}
