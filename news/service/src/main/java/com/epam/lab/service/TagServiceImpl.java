package com.epam.lab.service;

import com.epam.lab.builder.SearchSpecificationBuilderProducer;
import com.epam.lab.builder.SpecificationBuilder;
import com.epam.lab.dto.SearchCriteria;
import com.epam.lab.dto.SortCriteria;
import com.epam.lab.dto.TagDto;
import com.epam.lab.exception.TagAlreadyExistsException;
import com.epam.lab.model.Tag;
import com.epam.lab.repository.TagRepository;
import com.epam.lab.specification.SearchSpecification;
import com.epam.lab.specification.SortSpecification;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class TagServiceImpl implements TagService {
    private final TagRepository tagRepository;
    private final SearchSpecificationBuilderProducer<Tag> builderProducer;
    private final ModelMapper modelMapper;

    public TagServiceImpl(TagRepository tagRepository, SearchSpecificationBuilderProducer<Tag> builderProducer, ModelMapper modelMapper) {
        this.tagRepository = tagRepository;
        this.builderProducer = builderProducer;
        this.modelMapper = modelMapper;
    }

    @Override
    public List<TagDto> get(List<SearchCriteria> searchCriteriaList, List<SortCriteria> sortCriteriaList, int page, int numberPerPage) {
        List<SearchSpecification<Tag>> searchSpecifications = SpecificationBuilder.buildSearchSpecifications(searchCriteriaList, builderProducer);
        List<SortSpecification<Tag>> sortSpecifications = SpecificationBuilder.buildSortSpecifications(sortCriteriaList);
        int offset = page * numberPerPage;
        long totalNumber = tagRepository.count(searchSpecifications);
        if (offset >= totalNumber) {
            offset = (int) (totalNumber - 1) / numberPerPage * numberPerPage;
        }
        List<Tag> tags = tagRepository.find(searchSpecifications, sortSpecifications, offset, numberPerPage);
        return convertToDtoList(tags);
    }

    private List<TagDto> convertToDtoList(List<Tag> tags) {
        List<TagDto> tagDtoList = new ArrayList<>();
        tags.forEach(tag -> tagDtoList.add(modelMapper.map(tag, TagDto.class)));
        return tagDtoList;
    }

    @Override
    public Long add(TagDto tagDTO) {
        Tag tag = modelMapper.map(tagDTO, Tag.class);
        if (tagRepository.isTagAlreadyPersisted(tag)) {
            throw new TagAlreadyExistsException(tag.toString());
        }
        tagRepository.add(tag);
        return tag.getId();
    }

    @Override
    public void edit(TagDto tagDTO) {
        Tag tag = modelMapper.map(tagDTO, Tag.class);
        tagRepository.set(tag);
    }

    @Override
    public void delete(Long tagId) {
        tagRepository.remove(tagId);
    }

    @Override
    public TagDto get(Long id) {
        Tag tag = tagRepository.findOne(id);
        return modelMapper.map(tag, TagDto.class);
    }

    @Override
    public Long getQuantity(List<SearchCriteria> searchCriteriaList) {
        List<SearchSpecification<Tag>> searchSpecifications = SpecificationBuilder.buildSearchSpecifications(searchCriteriaList, builderProducer);
        return tagRepository.count(searchSpecifications);
    }
}
