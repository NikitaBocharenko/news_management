package com.epam.lab.service;

import com.epam.lab.dto.UserDto;

public interface UserService extends CrudService<UserDto> {
}
