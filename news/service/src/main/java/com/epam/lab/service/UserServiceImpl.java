package com.epam.lab.service;

import com.epam.lab.builder.SearchSpecificationBuilderProducer;
import com.epam.lab.builder.SpecificationBuilder;
import com.epam.lab.dto.SearchCriteria;
import com.epam.lab.dto.SortCriteria;
import com.epam.lab.dto.UserDto;
import com.epam.lab.exception.UserAlreadyExistsException;
import com.epam.lab.model.User;
import com.epam.lab.repository.UserRepository;
import com.epam.lab.specification.DefaultStringSearchSpecification;
import com.epam.lab.specification.QueryParameter;
import com.epam.lab.specification.SearchSpecification;
import com.epam.lab.specification.SortSpecification;
import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class UserServiceImpl implements UserDetailsService, UserService {
    private static final Logger LOG = Logger.getLogger(UserServiceImpl.class);
    private final UserRepository userRepository;
    private final SearchSpecificationBuilderProducer<User> builderProducer;
    private final PasswordEncoder passwordEncoder;
    private final ModelMapper modelMapper;

    public UserServiceImpl(UserRepository userRepository, SearchSpecificationBuilderProducer<User> builderProducer,
                           PasswordEncoder passwordEncoder, ModelMapper modelMapper) {
        this.userRepository = userRepository;
        this.builderProducer = builderProducer;
        this.passwordEncoder = passwordEncoder;
        this.modelMapper = modelMapper;
    }

    @Override
    public UserDto loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = this.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException(username);
        }
        UserDto userDto = modelMapper.map(user, UserDto.class);
        LOG.info(userDto);
        return userDto;
    }

    @Override
    public Long add(UserDto dto) {
        User user = modelMapper.map(dto, User.class);
        if (this.findByUsername(dto.getUsername()) != null) {
            throw new UserAlreadyExistsException(user.toString());
        }
        String passwordHash = passwordEncoder.encode(user.getPassword());
        user.setPassword(passwordHash);
        LOG.debug("User: " + user);
        userRepository.add(user);
        return user.getId();
    }

    private User findByUsername(String username) {
        SearchSpecification<User> specificationByLogin = new DefaultStringSearchSpecification<>(QueryParameter.USER_LOGIN, username);
        List<SearchSpecification<User>> searchSpecs = new ArrayList<>();
        searchSpecs.add(specificationByLogin);
        List<User> searchResult = userRepository.find(searchSpecs, new ArrayList<>(), 0, 1);
        if (searchResult.isEmpty()) {
            return null;
        }
        return searchResult.get(0);
    }

    @Override
    public void edit(UserDto dto) {
        User user = modelMapper.map(dto, User.class);
        String passwordHash;
        if (user.getPassword() == null) {
            passwordHash = userRepository.findOne(user.getId()).getPassword();
        } else {
            passwordHash = passwordEncoder.encode(user.getPassword());
        }
        user.setPassword(passwordHash);
        userRepository.set(user);
    }

    @Override
    public void delete(Long id) {
        userRepository.remove(id);
    }

    @Override
    public UserDto get(Long id) {
        User user = userRepository.findOne(id);
        return modelMapper.map(user, UserDto.class);
    }

    @Override
    public List<UserDto> get(List<SearchCriteria> searchCriteriaList, List<SortCriteria> sortCriteriaList, int page, int numberPerPage) {
        List<SearchSpecification<User>> searchSpecifications = SpecificationBuilder.buildSearchSpecifications(searchCriteriaList, builderProducer);
        List<SortSpecification<User>> sortSpecifications = SpecificationBuilder.buildSortSpecifications(sortCriteriaList);
        int offset = page * numberPerPage;
        long totalNumber = userRepository.count(searchSpecifications);
        if (offset >= totalNumber) {
            offset = (int) (totalNumber - 1) / numberPerPage * numberPerPage;
        }
        List<User> users = userRepository.find(searchSpecifications, sortSpecifications, offset, numberPerPage);
        return convertToDtoList(users);
    }

    private List<UserDto> convertToDtoList(List<User> users) {
        List<UserDto> userDtoList = new ArrayList<>();
        users.forEach(user -> userDtoList.add(modelMapper.map(user, UserDto.class)));
        return userDtoList;
    }

    @Override
    public Long getQuantity(List<SearchCriteria> searchCriteriaList) {
        List<SearchSpecification<User>> searchSpecifications = SpecificationBuilder.buildSearchSpecifications(searchCriteriaList, builderProducer);
        return userRepository.count(searchSpecifications);
    }
}
