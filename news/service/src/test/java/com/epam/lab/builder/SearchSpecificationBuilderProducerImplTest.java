package com.epam.lab.builder;

import com.epam.lab.model.News;
import com.epam.lab.specification.QueryParameter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(JUnit4.class)
public class SearchSpecificationBuilderProducerImplTest {
    private DefaultStringSearchSpecificationBuilder<News> expectedBuilder;
    private SearchSpecificationBuilderProducerImpl<News> builderProducer;
    private QueryParameter preparedParameter;

    @Before
    public void setUpSearchSpecificationBuilderProducerImplTest() {
        prepareExpectedBuilder();
        prepareQueryParameter();
        prepareBuilderProducer();
    }

    @Test
    public void shouldGetExpectedBuilder() {
        SearchSpecificationBuilder<News> actualBuilder = builderProducer.get(preparedParameter);
        assertEquals(expectedBuilder, actualBuilder);
    }

    @Test
    public void shouldGetNull() {
        SearchSpecificationBuilder<News> actualBuilder = builderProducer.get(QueryParameter.CREATION_DATE);
        assertNull(actualBuilder);
    }

    private void prepareExpectedBuilder() {
        expectedBuilder = new DefaultStringSearchSpecificationBuilder<>();
    }

    private void prepareQueryParameter() {
        preparedParameter = QueryParameter.TITLE;
    }

    private void prepareBuilderProducer() {
        builderProducer = new SearchSpecificationBuilderProducerImpl<>();
        builderProducer.add(preparedParameter, expectedBuilder);
    }
}
