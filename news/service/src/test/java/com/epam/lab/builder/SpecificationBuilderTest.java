package com.epam.lab.builder;

import com.epam.lab.dto.SearchCriteria;
import com.epam.lab.dto.SortCriteria;
import com.epam.lab.model.News;
import com.epam.lab.specification.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class SpecificationBuilderTest {
    private static final String DATE_STR_VALUE = "2020-01-02";
    private static final String TITLE_VALUE = "title";
    private static final String AUTHOR_NAME_VALUE = "name";
    private List<SearchCriteria> searchCriteriaList;
    private List<SortCriteria> sortCriteriaList;
    private List<SearchSpecification<News>> expectedSearchSpecifications;
    private List<SortSpecification<News>> expectedSortSpecifications;
    private SearchSpecificationBuilderProducerImpl<News> builderProducer;

    @Before
    public void setUpSpecificationBuilderTest() {
        prepareSearchCriteriaList();
        prepareSortCriteriaList();
        prepareExpectedSearchSpecifications();
        prepareExpectedSortSpecifications();
        prepareSpecificationBuilderProducer();
    }

    @Test
    public void shouldBuildSearchSpecifications() {
        List<SearchSpecification<News>> actualSearchSpecifications = SpecificationBuilder.buildSearchSpecifications(searchCriteriaList, builderProducer);
        assertEquals(expectedSearchSpecifications, actualSearchSpecifications);
    }

    @Test
    public void shouldBuildSortSpecifications() {
        List<SortSpecification<News>> actualSortSpecifications = SpecificationBuilder.buildSortSpecifications(sortCriteriaList);
        assertEquals(expectedSortSpecifications, actualSortSpecifications);
    }

    private void prepareSearchCriteriaList() {
        searchCriteriaList = new ArrayList<>();
        searchCriteriaList.add(new SearchCriteria(QueryParameter.TITLE, QueryOperator.EQUALS, TITLE_VALUE));
        searchCriteriaList.add(new SearchCriteria(QueryParameter.CREATION_DATE, QueryOperator.EQUALS, DATE_STR_VALUE));
        searchCriteriaList.add(new SearchCriteria(QueryParameter.AUTHOR_NAME, QueryOperator.EQUALS, AUTHOR_NAME_VALUE));
    }

    private void prepareSortCriteriaList() {
        sortCriteriaList = new ArrayList<>();
        sortCriteriaList.add(new SortCriteria(QueryParameter.TITLE, true));
        sortCriteriaList.add(new SortCriteria(QueryParameter.CREATION_DATE, false));
        sortCriteriaList.add(new SortCriteria(QueryParameter.MODIFICATION_DATE, true));
    }

    private void prepareExpectedSearchSpecifications() {
        expectedSearchSpecifications = new ArrayList<>();
        expectedSearchSpecifications.add(new DefaultStringSearchSpecification<>(QueryParameter.TITLE, TITLE_VALUE));
        LocalDate date = LocalDate.parse(DATE_STR_VALUE);
        expectedSearchSpecifications.add(new DefaultDateSearchSpecification<>(QueryParameter.CREATION_DATE, date));
        expectedSearchSpecifications.add(new NewsByAuthorStringSearchSpecification(QueryParameter.AUTHOR_NAME, AUTHOR_NAME_VALUE));
    }

    private void prepareExpectedSortSpecifications() {
        expectedSortSpecifications = new ArrayList<>();
        expectedSortSpecifications.add(new SortSpecificationImpl<>(QueryParameter.TITLE, true));
        expectedSortSpecifications.add(new SortSpecificationImpl<>(QueryParameter.CREATION_DATE, false));
        expectedSortSpecifications.add(new SortSpecificationImpl<>(QueryParameter.MODIFICATION_DATE, true));
    }

    private void prepareSpecificationBuilderProducer() {
        builderProducer = new SearchSpecificationBuilderProducerImpl<>();
        builderProducer.add(QueryParameter.TITLE, new DefaultStringSearchSpecificationBuilder<>());
        builderProducer.add(QueryParameter.SHORT_TEXT, new DefaultStringSearchSpecificationBuilder<>());
        builderProducer.add(QueryParameter.FULL_TEXT, new DefaultStringSearchSpecificationBuilder<>());
        builderProducer.add(QueryParameter.CREATION_DATE, new DefaultDateSearchSpecificationBuilder<>());
        builderProducer.add(QueryParameter.MODIFICATION_DATE, new DefaultDateSearchSpecificationBuilder<>());
        builderProducer.add(QueryParameter.AUTHOR_NAME, new NewsByAuthorStringSearchSpecificationBuilder());
        builderProducer.add(QueryParameter.AUTHOR_SURNAME, new NewsByAuthorStringSearchSpecificationBuilder());
        builderProducer.add(QueryParameter.TAG_NAME, new NewsByTagStringSearchSpecificationBuilder());
    }
}
