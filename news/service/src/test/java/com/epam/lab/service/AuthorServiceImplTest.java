package com.epam.lab.service;

import com.epam.lab.builder.SearchSpecificationBuilderProducer;
import com.epam.lab.dto.AuthorDto;
import com.epam.lab.model.Author;
import com.epam.lab.repository.AuthorRepository;
import com.epam.lab.repository.AuthorRepositoryImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.*;

@RunWith(JUnit4.class)
public class AuthorServiceImplTest {
    private AuthorRepository mockAuthorJpaRepository;
    private SearchSpecificationBuilderProducer<Author> mockBuilderProducer;
    private AuthorService authorService;
    private List<AuthorDto> expectedAuthors;
    private AuthorDto expectedAuthor;
    private List<Author> authorEntityList;
    private Author authorEntity;
    private ModelMapper mockModelMapper;

    @Before
    public void setUpAuthorServiceTest() {
        prepareAuthorEntity();
        prepareAuthorDTO();
        prepareAuthorEntityList();
        prepareAuthorDTOList();
        prepareMockAuthorRepository();
        prepareMockBuilderProducer();
        prepareMockModelMapper();
        prepareAuthorService();
    }

    @Test
    public void shouldFindAuthors() {
        List<AuthorDto> actualAuthors = authorService.get(new ArrayList<>(), new ArrayList<>(), 13, 14);
        assertEquals(expectedAuthors, actualAuthors);
    }

    @Test
    public void shouldFindOneAuthor() {
        AuthorDto actualAuthor = authorService.get(1L);
        verify(mockAuthorJpaRepository, times(1)).findOne(1L);
        assertEquals(expectedAuthor, actualAuthor);
    }

    @Test
    public void shouldAddAuthor() {
        authorService.add(expectedAuthor);
        verify(mockAuthorJpaRepository, times(1)).add(authorEntity);
    }

    @Test
    public void shouldEditAuthor() {
        authorService.edit(expectedAuthor);
        verify(mockAuthorJpaRepository, times(1)).set(authorEntity);
    }

    @Test
    public void shouldDeleteAuthor() {
        authorService.delete(1L);
        verify(mockAuthorJpaRepository, times(1)).remove(1L);
    }

    @Test
    public void shouldCountTags() {
        long actual = authorService.getQuantity(new ArrayList<>());
        assertEquals(3, actual);
    }

    private void prepareAuthorEntity() {
        authorEntity = new Author(1L, "TestName1", "TestSurname1");
    }

    private void prepareAuthorDTO() {
        expectedAuthor = new AuthorDto(authorEntity);
    }

    private void prepareAuthorEntityList() {
        authorEntityList = new ArrayList<>();
        authorEntityList.add(authorEntity);
        authorEntityList.add(new Author(2L, "TestName2", "TestSurname2"));
        authorEntityList.add(new Author(3L, "TestName3", "TestSurname3"));
    }

    private void prepareAuthorDTOList() {
        expectedAuthors = new ArrayList<>();
        authorEntityList.forEach(author -> {
            AuthorDto authorDTO = new AuthorDto(author);
            expectedAuthors.add(authorDTO);
        });
    }

    private void prepareMockAuthorRepository() {
        mockAuthorJpaRepository = mock(AuthorRepositoryImpl.class);
        when(mockAuthorJpaRepository.find(any(List.class), any(List.class), anyInt(), anyInt())).thenReturn(authorEntityList);
        when(mockAuthorJpaRepository.findOne(anyLong())).thenReturn(authorEntity);
        when(mockAuthorJpaRepository.count(any(List.class))).thenReturn(3L);
    }

    private void prepareMockBuilderProducer() {
        mockBuilderProducer = mock(SearchSpecificationBuilderProducer.class);
    }

    private void prepareMockModelMapper() {
        mockModelMapper = mock(ModelMapper.class);
        when(mockModelMapper.map(authorEntityList.get(0), AuthorDto.class)).thenReturn(expectedAuthors.get(0));
        when(mockModelMapper.map(authorEntityList.get(1), AuthorDto.class)).thenReturn(expectedAuthors.get(1));
        when(mockModelMapper.map(authorEntityList.get(2), AuthorDto.class)).thenReturn(expectedAuthors.get(2));
        when(mockModelMapper.map(expectedAuthor, Author.class)).thenReturn(authorEntity);
    }

    private void prepareAuthorService() {
        authorService = new AuthorServiceImpl(mockAuthorJpaRepository, mockBuilderProducer, mockModelMapper);
    }
}
