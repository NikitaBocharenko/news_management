package com.epam.lab.service;

import com.epam.lab.builder.SearchSpecificationBuilderProducer;
import com.epam.lab.dto.NewsDto;
import com.epam.lab.model.Author;
import com.epam.lab.model.News;
import com.epam.lab.model.Tag;
import com.epam.lab.repository.AuthorRepositoryImpl;
import com.epam.lab.repository.NewsRepository;
import com.epam.lab.repository.NewsRepositoryImpl;
import com.epam.lab.repository.TagRepositoryImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.modelmapper.ModelMapper;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.*;

@RunWith(JUnit4.class)
public class NewsServiceImplTest {
    private NewsRepository mockNewsJpaRepository;
    private AuthorRepositoryImpl mockAuthorJpaRepository;
    private TagRepositoryImpl mockTagJpaRepository;
    private SearchSpecificationBuilderProducer<News> mockBuilderProducer;
    private NewsService newsService;
    private List<NewsDto> expectedNewsList;
    private NewsDto expectedNews;
    private News newsEntity;
    private Author authorEntity;
    private Tag tagEntity;
    private List<News> newsEntityList;
    private Set<Tag> tagSet;
    private ModelMapper mockModelMapper;

    @Before
    public void setUpNewsService() {
        prepareAuthorEntity();
        prepareTagEntity();
        prepareTagSet();
        prepareNewsEntity();
        prepareExpectedNews();
        prepareNewsEntityList();
        prepareExpectedNewsList();
        prepareMockNewsRepository();
        prepareMockAuthorRepository();
        prepareMockTagRepository();
        prepareMockBuilderProducer();
        prepareMockModelMapper();
        prepareNewsService();
    }

    @Test
    public void shouldFindNews() {
        List<NewsDto> actualNewsList = newsService.get(new ArrayList<>(), new ArrayList<>(), 23, 42);
        assertEquals(expectedNewsList, actualNewsList);
    }

    @Test
    public void shouldFindOneNews() {
        NewsDto actualNews = newsService.get(1L);
        verify(mockNewsJpaRepository, times(1)).findOne(1L);
        assertEquals(expectedNews, actualNews);
    }

    @Test
    public void shouldAddNewsWhenAuthorAndTagsExists() {
        newsService.add(expectedNews);
        verify(mockNewsJpaRepository, times(1)).add(newsEntity);
        verify(mockAuthorJpaRepository, never()).add(authorEntity);
        verify(mockTagJpaRepository, never()).add(tagEntity);
    }

    @Test
    public void shouldAddNewsWhenAuthorAndTagsNotExists() {
        Author notExistingAuthorEntity = new Author(null, "Testname", "Testsurname");
        Tag notExistingTagEntity = new Tag(null, "testtag");
        Set<Tag> notExistingTagEntities = new HashSet<>();
        notExistingTagEntities.add(notExistingTagEntity);
        News notExistingNewsEntity = new News(null, "Title 1", "Short text 1", "Full text 1", LocalDate.now(), LocalDate.now(), notExistingAuthorEntity, notExistingTagEntities);
        NewsDto notExistingNewsDTO = new NewsDto(notExistingNewsEntity, notExistingAuthorEntity, notExistingTagEntities);
        when(mockModelMapper.map(notExistingNewsDTO.getAuthorDto(), Author.class)).thenReturn(notExistingAuthorEntity);
        when(mockModelMapper.map(notExistingNewsDTO.getTagDtoSet().toArray()[0], Tag.class)).thenReturn(notExistingTagEntity);
        when(mockModelMapper.map(notExistingNewsDTO, News.class)).thenReturn(notExistingNewsEntity);

        newsService.add(notExistingNewsDTO);
        verify(mockNewsJpaRepository, times(1)).add(notExistingNewsEntity);
        verify(mockAuthorJpaRepository, times(1)).add(notExistingAuthorEntity);
        verify(mockTagJpaRepository, times(1)).add(notExistingTagEntity);
    }

    @Test
    public void shouldEditNews() {
        newsService.edit(expectedNews);
        verify(mockNewsJpaRepository, times(1)).set(newsEntity);
    }

    @Test
    public void shouldDeleteNews() {
        newsService.delete(1L);
        verify(mockNewsJpaRepository, times(1)).remove(1L);
    }

    @Test
    public void shouldCountNews() {
        long actual = newsService.getQuantity(new ArrayList<>());
        assertEquals(3, actual);
    }

    private void prepareAuthorEntity() {
        authorEntity = new Author(1L, "Testname", "Testsurname");
    }

    private void prepareTagEntity() {
        tagEntity = new Tag(1L, "testtag");
    }

    private void prepareNewsEntity() {
        newsEntity = new News(1L, "Title 1", "Short text 1", "Full text 1", LocalDate.now(), LocalDate.now(), authorEntity, tagSet);
    }

    private void prepareTagSet() {
        tagSet = new HashSet<>();
        tagSet.add(tagEntity);
    }

    private void prepareExpectedNews() {
        expectedNews = new NewsDto(newsEntity, authorEntity, tagSet);
    }

    private void prepareNewsEntityList() {
        newsEntityList = new ArrayList<>();
        newsEntityList.add(newsEntity);
        newsEntityList.add(new News(2L, "Title 2", "Short text 2", "Full text 2", LocalDate.now(), LocalDate.now(), authorEntity, tagSet));
        newsEntityList.add(new News(3L, "Title 3", "Short text 3", "Full text 3", LocalDate.now(), LocalDate.now(), authorEntity, tagSet));
    }

    private void prepareExpectedNewsList() {
        expectedNewsList = new ArrayList<>();
        newsEntityList.forEach(news -> expectedNewsList.add(new NewsDto(news, authorEntity, tagSet)));
    }

    private void prepareMockNewsRepository() {
        mockNewsJpaRepository = mock(NewsRepositoryImpl.class);
        when(mockNewsJpaRepository.find(any(List.class), any(List.class), anyInt(), anyInt())).thenReturn(newsEntityList);
        when(mockNewsJpaRepository.findOne(anyLong())).thenReturn(newsEntity);
        when(mockNewsJpaRepository.count(any(List.class))).thenReturn(3L);
    }

    private void prepareMockAuthorRepository() {
        mockAuthorJpaRepository = mock(AuthorRepositoryImpl.class);
        when(mockAuthorJpaRepository.findOne(anyLong())).thenReturn(authorEntity);
    }

    private void prepareMockTagRepository() {
        mockTagJpaRepository = mock(TagRepositoryImpl.class);
        when(mockTagJpaRepository.findOne(anyLong())).thenReturn(tagEntity);
    }

    private void prepareMockBuilderProducer() {
        mockBuilderProducer = mock(SearchSpecificationBuilderProducer.class);
    }

    private void prepareMockModelMapper() {
        mockModelMapper = mock(ModelMapper.class);
        when(mockModelMapper.map(newsEntityList.get(0), NewsDto.class)).thenReturn(expectedNewsList.get(0));
        when(mockModelMapper.map(newsEntityList.get(1), NewsDto.class)).thenReturn(expectedNewsList.get(1));
        when(mockModelMapper.map(newsEntityList.get(2), NewsDto.class)).thenReturn(expectedNewsList.get(2));
        when(mockModelMapper.map(expectedNews, News.class)).thenReturn(newsEntity);
        when(mockModelMapper.map(expectedNews.getAuthorDto(), Author.class)).thenReturn(authorEntity);
        when(mockModelMapper.map(expectedNews.getTagDtoSet().toArray()[0], Tag.class)).thenReturn(tagEntity);
    }

    private void prepareNewsService() {
        newsService = new NewsServiceImpl(mockNewsJpaRepository, mockAuthorJpaRepository, mockTagJpaRepository, mockBuilderProducer, mockModelMapper);
    }
}
