package com.epam.lab.service;

import com.epam.lab.builder.SearchSpecificationBuilderProducer;
import com.epam.lab.dto.TagDto;
import com.epam.lab.model.Tag;
import com.epam.lab.repository.TagRepository;
import com.epam.lab.repository.TagRepositoryImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.*;

@RunWith(JUnit4.class)
public class TagServiceImplTest {
    private TagRepository mockTagJpaRepository;
    private SearchSpecificationBuilderProducer<Tag> mockBuilderProducer;
    private TagService tagService;
    private List<TagDto> expectedTagList;
    private TagDto expectedTag;
    private Tag tagEntity;
    private List<Tag> tagEntityList;
    private ModelMapper mockModelMapper;

    @Before
    public void setUpTagService() {
        prepareTagEntity();
        prepareTagEntityList();
        prepareExpectedTag();
        prepareExpectedTagList();
        prepareMockTagRepository();
        prepareMockSpecificationBuilder();
        prepareMockModelMapper();
        prepareTagService();
    }

    @Test
    public void shouldFindTags() {
        List<TagDto> actualTags = tagService.get(new ArrayList<>(), new ArrayList<>(), 5, 6);
        assertEquals(expectedTagList, actualTags);
    }

    @Test
    public void shouldFindOneTag() {
        TagDto actualTag = tagService.get(1L);
        verify(mockTagJpaRepository, times(1)).findOne(1L);
        assertEquals(expectedTag, actualTag);
    }

    @Test
    public void shouldAddTag() {
        tagService.add(expectedTag);
        verify(mockTagJpaRepository, times(1)).add(tagEntity);
    }

    @Test
    public void shouldEditTag() {
        tagService.edit(expectedTag);
        verify(mockTagJpaRepository, times(1)).set(tagEntity);
    }

    @Test
    public void shouldDeleteTag() {
        tagService.delete(1L);
        verify(mockTagJpaRepository, times(1)).remove(1L);
    }

    @Test
    public void shouldCountTags() {
        long actual = tagService.getQuantity(new ArrayList<>());
        assertEquals(3, actual);
    }

    private void prepareTagEntity() {
        tagEntity = new Tag(1L, "TestName1");
    }

    private void prepareTagEntityList() {
        tagEntityList = new ArrayList<>();
        tagEntityList.add(tagEntity);
        tagEntityList.add(new Tag(2L, "TestName2"));
        tagEntityList.add(new Tag(3L, "TestName3"));
    }

    private void prepareExpectedTag() {
        expectedTag = new TagDto(tagEntity);
    }

    private void prepareExpectedTagList() {
        expectedTagList = new ArrayList<>();
        tagEntityList.forEach(tag -> expectedTagList.add(new TagDto(tag)));
    }

    private void prepareMockTagRepository() {
        mockTagJpaRepository = mock(TagRepositoryImpl.class);
        when(mockTagJpaRepository.find(any(List.class), any(List.class), anyInt(), anyInt())).thenReturn(tagEntityList);
        when(mockTagJpaRepository.findOne(anyLong())).thenReturn(tagEntity);
        when(mockTagJpaRepository.count(any(List.class))).thenReturn(3L);
    }

    private void prepareMockSpecificationBuilder() {
        mockBuilderProducer = mock(SearchSpecificationBuilderProducer.class);
    }

    private void prepareMockModelMapper() {
        mockModelMapper = mock(ModelMapper.class);
        when(mockModelMapper.map(tagEntityList.get(0), TagDto.class)).thenReturn(expectedTagList.get(0));
        when(mockModelMapper.map(tagEntityList.get(1), TagDto.class)).thenReturn(expectedTagList.get(1));
        when(mockModelMapper.map(tagEntityList.get(2), TagDto.class)).thenReturn(expectedTagList.get(2));
        when(mockModelMapper.map(expectedTag, Tag.class)).thenReturn(tagEntity);
    }

    private void prepareTagService() {
        tagService = new TagServiceImpl(mockTagJpaRepository, mockBuilderProducer, mockModelMapper);
    }
}
