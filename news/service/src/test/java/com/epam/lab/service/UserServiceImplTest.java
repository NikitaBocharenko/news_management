package com.epam.lab.service;

import com.epam.lab.builder.SearchSpecificationBuilderProducer;
import com.epam.lab.dto.UserDto;
import com.epam.lab.exception.UserAlreadyExistsException;
import com.epam.lab.model.Role;
import com.epam.lab.model.User;
import com.epam.lab.repository.UserRepository;
import com.epam.lab.repository.UserRepositoryImpl;
import com.epam.lab.specification.DefaultStringSearchSpecification;
import com.epam.lab.specification.QueryParameter;
import com.epam.lab.specification.SearchSpecification;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.modelmapper.ModelMapper;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.*;

@RunWith(JUnit4.class)
public class UserServiceImplTest {
    private UserRepository mockUserJpaRepository;
    private SearchSpecificationBuilderProducer<User> mockBuilderProducer;
    private UserServiceImpl userService;
    private List<UserDto> expectedUserList;
    private UserDto expectedUser;
    private Set<Role> roleSet;
    private User userEntity;
    private List<User> userEntityList;
    private PasswordEncoder mockPasswordEncoder;
    private ModelMapper mockModelMapper;

    @Before
    public void setUpUserService() {
        prepareRoleSet();
        prepareUserEntity();
        prepareUserEntityList();
        prepareExpectedUser();
        prepareExpectedUserList();
        prepareMockUserRepository();
        prepareMockSpecificationBuilder();
        prepareMockPasswordEncoder();
        prepareMockModelMapper();
        prepareUserService();
    }

    @Test
    public void shouldLoadByUsername() {
        UserDto actual = userService.loadUserByUsername("mikita");
        assertEquals(expectedUser, actual);
    }

    @Test(expected = UsernameNotFoundException.class)
    public void shouldThrowUsernameNotFoundException() {
        UserDto actual = userService.loadUserByUsername("TestLogin4");
        fail("UsernameNotFoundException must be thrown");
    }

    @Test
    public void shouldFindUsers() {
        List<UserDto> actualUsers = userService.get(new ArrayList<>(), new ArrayList<>(), 5, 6);
        assertEquals(expectedUserList, actualUsers);
    }

    @Test
    public void shouldFindOneUser() {
        UserDto actualUser = userService.get(1L);
        verify(mockUserJpaRepository, times(1)).findOne(1L);
        assertEquals(expectedUser, actualUser);
    }

    @Test
    public void shouldAddUser() {
        User expectedAddedUser = new User(4L, "TestName4", "TestSurname4", "TestLogin4", "TestPassword4", roleSet);
        UserDto newUser = new UserDto(expectedAddedUser);
        when(mockModelMapper.map(newUser, User.class)).thenReturn(expectedAddedUser);

        userService.add(newUser);
        verify(mockUserJpaRepository, times(1)).add(expectedAddedUser);
        verify(mockPasswordEncoder, times(1)).encode("TestPassword4");
    }

    @Test(expected = UserAlreadyExistsException.class)
    public void shouldThrowUserAlreadyExistsException() {
        userService.add(expectedUser);
        fail("UserAlreadyExistsException must be thrown");
    }

    @Test
    public void shouldEditUser() {
        userService.edit(expectedUser);
        verify(mockUserJpaRepository, times(1)).set(userEntity);
        verify(mockPasswordEncoder, times(1)).encode(expectedUser.getPassword());
    }

    @Test
    public void shouldDeleteUser() {
        userService.delete(1L);
        verify(mockUserJpaRepository, times(1)).remove(1L);
    }

    @Test
    public void shouldCountUsers() {
        long actual = userService.getQuantity(new ArrayList<>());
        assertEquals(3, actual);
    }

    private void prepareRoleSet() {
        roleSet = new HashSet<>();
        roleSet.add(new Role(1L, "ADMIN"));
    }

    private void prepareUserEntity() {
        userEntity = new User(1L, "Mikita", "Bacharenka", "mikita", "111", roleSet);
    }

    private void prepareUserEntityList() {
        userEntityList = new ArrayList<>();
        userEntityList.add(userEntity);
        Set<Role> roles = new HashSet<>();
        roles.add(new Role(2L, "USER"));
        userEntityList.add(new User(2L, "TestName2", "TestSurname2", "TestLogin2", "TestPassword2", roles));
        roles = new HashSet<>();
        roles.add(new Role(3L, "GUEST"));
        userEntityList.add(new User(3L, "TestName3", "TestSurname3", "TestLogin3", "TestPassword3", roles));
    }

    private void prepareExpectedUser() {
        expectedUser = new UserDto(userEntity);
    }

    private void prepareExpectedUserList() {
        expectedUserList = new ArrayList<>();
        userEntityList.forEach(user -> expectedUserList.add(new UserDto(user)));
    }

    private void prepareMockUserRepository() {
        mockUserJpaRepository = mock(UserRepositoryImpl.class);
        when(mockUserJpaRepository.find(eq(new ArrayList<>()), eq(new ArrayList<>()), anyInt(), anyInt())).thenReturn(userEntityList);
        List<SearchSpecification<User>> searchSpecs = new ArrayList<>();
        searchSpecs.add(new DefaultStringSearchSpecification<>(QueryParameter.USER_LOGIN, "TestLogin4"));
        when(mockUserJpaRepository.find(eq(searchSpecs), eq(new ArrayList<>()), eq(0), eq(1))).thenReturn(new ArrayList<>());
        searchSpecs = new ArrayList<>();
        searchSpecs.add(new DefaultStringSearchSpecification<>(QueryParameter.USER_LOGIN, "mikita"));
        when(mockUserJpaRepository.find(eq(searchSpecs), eq(new ArrayList<>()), eq(0), eq(1))).thenReturn(userEntityList);
        when(mockUserJpaRepository.findOne(anyLong())).thenReturn(userEntity);
        when(mockUserJpaRepository.count(any(List.class))).thenReturn(3L);
    }

    private void prepareMockSpecificationBuilder() {
        mockBuilderProducer = mock(SearchSpecificationBuilderProducer.class);
    }

    private void prepareMockPasswordEncoder() {
        mockPasswordEncoder = mock(PasswordEncoder.class);
        when(mockPasswordEncoder.encode("TestPassword4")).thenReturn("TestPassword4");
        when(mockPasswordEncoder.encode("111")).thenReturn("111");
    }

    private void prepareMockModelMapper() {
        mockModelMapper = mock(ModelMapper.class);
        when(mockModelMapper.map(userEntityList.get(0), UserDto.class)).thenReturn(expectedUserList.get(0));
        when(mockModelMapper.map(userEntityList.get(1), UserDto.class)).thenReturn(expectedUserList.get(1));
        when(mockModelMapper.map(userEntityList.get(2), UserDto.class)).thenReturn(expectedUserList.get(2));
        when(mockModelMapper.map(expectedUser, User.class)).thenReturn(userEntity);
    }

    private void prepareUserService() {
        userService = new UserServiceImpl(mockUserJpaRepository, mockBuilderProducer, mockPasswordEncoder, mockModelMapper);
    }
}
