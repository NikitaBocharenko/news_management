﻿# News management
Данное веб приложение было разработано во время обучения в Java лаборатории компании EPAM Belarus.
## Business process
Приложение позволяет просматривать и редактировать список новостей (News). У каждой новости есть один автор (Author) и любое количество тегов (Tag). Новости можно искать по автору, тегам и дате создания, а также сортировать по любому признаку.

В системе 2 роли (Role) пользователей (User): обычный пользователь (может редактировать список новостей, а также списки авторов и тегов), и администратор (может то же, что и пользователь, а также управляет списком пользователей). Незарегистрированный пользователь может только просматривать список новостей.
## Technologies
**Back:** Java, Spring Framework, JPA/Hibernate, REST API, Spring Security, HTTP, Junit, Mockito, Tomcat, Maven

**DB:** PostgreSQL

**Front:** React, JavaScript, HTML, Bootstrap
## Structure
Веб приложение состоит из двух модулей: 

 - [Back](/news)
 - [Front](/ui)

В свою очередь, модуль **Back** также разделен на три подмодуля: 

 - [Repository-jpa-hibernate](/news/repository-jpa-hibernate)
 - [Service](/news/service)
 - [Controller](/news/controller)

