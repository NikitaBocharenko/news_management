import './App.css';
import React from 'react'
import {
    BrowserRouter as Router,
    Switch,
    Route
  } from "react-router-dom";
import Header from './component/Header'
import Footer from './component/Footer'
import ListTagsComponent from './component/ListTagsComponent'
import TagComponent from './component/TagComponent'
import ListNewsComponent from './component/ListNewsComponent'
import NewsComponent from './component/NewsComponent'
import ListAuthorsComponent from './component/ListAuthorsComponent'
import AuthorComponent from './component/AuthorComponent'
import Login from './component/Login'

class App extends React.Component {
  componentDidMount() {
    document.body.style.paddingTop = '60px';
    document.body.style.paddingBottom = '60px';
  }

  renderPage() {
    const userName = sessionStorage.getItem("userName");
    if (userName) {
      return (
          <div className="container-fluid">
            <Header />
            <Router>
                <Switch>
                    <Route path="/" exact component={ListNewsComponent} />
                    <Route path="/news" exact component={ListNewsComponent} />
                    <Route path="/news/:id" component={NewsComponent} />
                    <Route path="/authors" exact component={ListAuthorsComponent} />
                    <Route path="/authors/:id" component={AuthorComponent} />
                    <Route path="/tags" exact component={ListTagsComponent} />
                    <Route path="/tags/:id" component={TagComponent} />
                </Switch>
            </Router>
            <Footer />
          </div>
      );
    } else {
      return(
        <Login />
      );
    }
  }

  render() {
      return (
        <>
          {this.renderPage()}
        </>
      )
  }
}
  
export default App;