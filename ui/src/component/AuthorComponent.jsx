import React from 'react'
import AuthorDataService from '../service/AuthorDataService'
import { Formik, Field, Form, ErrorMessage} from 'formik'
import { withTranslation } from 'react-i18next';
import Sidebar from './Sidebar';

class AuthorComponent extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            id: this.props.match.params.id,
            name: "",
            surname: ""
        }
        this.onSubmit = this.onSubmit.bind(this)
        this.validate = this.validate.bind(this)
    }

    componentDidMount() {
        console.log(this.state.id)
        if (this.state.id == -1) {
            return;
        }
        AuthorDataService.getById(this.state.id)
            .then(
                response => {
                    console.log(response.data);
                    this.setState({
                        name: response.data.name,
                        surname: response.data.surname});
            });
    }

    onSubmit(values) {
        console.log(values);
        let author = {
            name: values.name,
            surname: values.surname
        }
        if (this.state.id == -1) {
            AuthorDataService.add(author)
                .then(() => this.props.history.push("/authors"));
        } else {
            author.id = this.state.id;
            AuthorDataService.edit(author)
                .then(() => this.props.history.push("/authors"));
        }
    }

    validate(values) {
        let errors = {}
        if (!values.name) {
            errors.name = this.props.t("validation.author.name.empty")
        } else if (values.name.length > 30) {
            errors.name = this.props.t("validation.author.name.overlong");
        }
        if (!values.surname) {
            errors.surname = this.props.t("validation.author.surname.empty");
        } else if (values.surname.length > 30) {
            errors.surname = this.props.t("validation.author.surname.overlong");
        }
        return errors
    }

    render() {
        let { name, surname } = this.state;
        let sidebarButtons = [
            {
                title: this.props.t("component.saveBtn"),
                class: "btn btn-success",
                type: "submit",
                form: "authorForm"
            },
            {
                title: this.props.t("component.resetBtn"),
                class: "btn btn-warning",
                type: "reset",
                form: "authorForm"
            },
            {
                title: this.props.t("component.cancel"),
                class: "btn btn-danger",
                onClick: () => {window.history.back()}
            }
        ];
        return (
            <div className="row">
                <div className="col-2">
                    <Sidebar buttons={sidebarButtons}/>
                </div>
                <div className="col-8">
                    <h3 className="ml-4 my-3">{this.props.t("author.entityName")}</h3>
                    <Formik initialValues={{ name, surname }} 
                            onSubmit={this.onSubmit}
                            validateOnChange={false}
                            validateOnBlur={false}
                            validate={this.validate} 
                            enableReinitialize={true}>
                    {(props) => (
                            <Form id="authorForm">
                                <fieldset className="form-group">
                                    <label>{this.props.t("author.name")}</label>
                                    <Field className="form-control" type="text" name="name" />
                                </fieldset>
                                <ErrorMessage name="name" component="div"
                                    className="alert alert-warning" />
                                <fieldset className="form-group">
                                    <label>{this.props.t("author.surname")}</label>
                                    <Field className="form-control" type="text" name="surname" />
                                </fieldset>
                                <ErrorMessage name="surname" component="div"
                                    className="alert alert-warning" />
                            </Form>
                        )
                    }</Formik>
                </div>
            </div>
        )
    }
}

export default withTranslation()(AuthorComponent)