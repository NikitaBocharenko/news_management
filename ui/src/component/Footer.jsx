import React from 'react'
import { withTranslation } from 'react-i18next';

class Footer extends React.Component {
    render(){
        return(
            <nav style={{left: "50%", "margin-left": "-37.5%", height: "60px"}} class="navbar fixed-bottom navbar-light bg-light w-75 d-flex justify-content-center">
                {this.props.t("footer.content")}
            </nav>
        )
    }
}

export default withTranslation()(Footer)