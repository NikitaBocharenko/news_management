import React from 'react'
import LanguageSwitcher from '../service/LanguageSwitcher'
import { withTranslation } from 'react-i18next';

class Header extends React.Component {
    getListClass(link) {
        if (link === "/news" && window.location.pathname === "/" || link === window.location.pathname) {
            return ("nav-item active");
        } else {
            return ("nav-item");
        }
    }

    logOut() {
        sessionStorage.removeItem('userName');
        window.location.reload();
    }

    render() {
        console.log("RENDER HEADER");
        return (
            <nav style={{left: "50%", "margin-left": "-37.5%"}} className="navbar navbar-expand-lg navbar-light bg-light fixed-top w-75">
                <a className="navbar-brand" href="/">{this.props.t("header.title")}</a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul className="navbar-nav">
                    <li className={this.getListClass("/news")}>
                        <a className="nav-link" href="/news">{this.props.t("header.newsLink")}</a>
                    </li>
                    <li className={this.getListClass("/authors")}>
                        <a className="nav-link" href="/authors">{this.props.t("header.authorsLink")}</a>
                    </li>
                    <li className={this.getListClass("/tags")}>
                        <a className="nav-link" href="/tags">{this.props.t("header.tagsLink")}</a>
                    </li>
                    <li className="nav-item dropdown">
                        <a className="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {this.props.t("language.title")}    
                        </a>
                        <div className="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a className="dropdown-item" href="#" onClick={()=>LanguageSwitcher.changeLanguage("en")}>{this.props.t("language.en")}</a>
                            <a className="dropdown-item" href="#" onClick={()=>LanguageSwitcher.changeLanguage("ru")}>{this.props.t("language.ru")}</a>
                        </div>
                    </li>
                    </ul>
                </div>
                <div className="mr-3">
                    {sessionStorage.getItem('userName')}
                </div>
                <div>
                    <button type="button" className="btn btn-dark" onClick={this.logOut}>{this.props.t("login.logout")}</button>
                </div>
            </nav>
        )
    }
}

export default withTranslation()(Header)