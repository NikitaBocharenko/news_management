import React from 'react';
import AuthorDataService from '../service/AuthorDataService';
import Pagination from './Pagination';
import Sidebar from './Sidebar';
import { withTranslation } from 'react-i18next';

class ListAuthorsComponent extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            authors: [],
            page: 0,
            numberPerPage: 10,
            totalEntityNumber: -1,
            message: null
        }
        this.refreshAuthors = this.refreshAuthors.bind(this)
        this.addAuthorClicked = this.addAuthorClicked.bind(this)
    }

    componentDidMount() {
        this.refreshAuthors(this.state.page);
    }

    refreshAuthors(currentPage, newNumberPerPage) {
        if (!newNumberPerPage) {
            newNumberPerPage = this.state.numberPerPage;
        }
        AuthorDataService.getAll(currentPage, newNumberPerPage)
            .then(
                response => {
                    let newTotalEntityNumber = Number.parseInt(response.headers.totalentitycount);
                    let pageQuantity = Math.ceil(newTotalEntityNumber / newNumberPerPage);
                    if (currentPage >= pageQuantity) {
                        currentPage = pageQuantity - 1;
                    }
                    this.setState({
                        authors: response.data,
                        totalEntityNumber: newTotalEntityNumber,
                        page: currentPage,
                        numberPerPage: newNumberPerPage
                    });
                }
            )
    }

    deleteAuthorClicked(id) {
        AuthorDataService.delete(id)
            .then(
                response => {
                    this.setState({ message: this.props.t("list.authorDeletedMessage", {deletedId: id}) })
                    this.refreshAuthors()
                }
            )
    }

    updateAuthorClicked(id) {
        this.props.history.push("/authors/" + id);
    }

    addAuthorClicked() {
        this.props.history.push("/authors/-1")
    }

    render() {
        const authorsRender = this.state.authors.map(author => {
            return (
                <tr key = {author.id}>
                    <td>{author.id}</td>
                    <td>{author.name}</td>
                    <td>{author.surname}</td>
                    <td>
                        <button className="btn btn-warning" 
                                onClick={() => this.deleteAuthorClicked(author.id)}
                                >{this.props.t("list.deleteBtn")}
                        </button>
                    </td>
                    <td>
                        <button className="btn btn-success" 
                                onClick={() => this.updateAuthorClicked(author.id)}
                                >{this.props.t("list.updateBtn")}
                        </button>
                    </td>
                </tr>
            )
        });
        let sidebarButtons = [
            {
                title: this.props.t("author.add"),
                class: "btn btn-success",
                onClick: () => {window.location.href = "/authors/-1"}
            }
        ];
        return (
            <div className="row">
                <div className="col-2">
                    <Sidebar buttons={sidebarButtons}/>
                </div>
                <div className="col-8">
                    <h3 className="ml-4 my-3">{this.props.t("list.authorTitle")}</h3>
                    {this.state.message && <div class="alert alert-success">{this.state.message}</div>}
                    <table className="table">
                        <thead>
                            <tr>
                                <th>{this.props.t("author.id")}</th>
                                <th>{this.props.t("author.name")}</th>
                                <th>{this.props.t("author.surname")}</th>
                                <th colSpan="2">{this.props.t("list.actions")}</th>
                            </tr>
                        </thead>
                        <tbody>
                            {authorsRender}
                        </tbody>
                    </table>
                    <Pagination currentPage={this.state.page} totalEntityNumber={this.state.totalEntityNumber} numberPerPage={this.state.numberPerPage} changePage={this.refreshAuthors}/>
                </div>
                <div className="col-2"></div>
            </div>
        )
    }
}

export default withTranslation()(ListAuthorsComponent)