import React from 'react';
import NewsDataService from '../service/NewsDataService';
import Pagination from './Pagination';
import Sidebar from './Sidebar';
import { withTranslation } from 'react-i18next';

class ListNewsComponent extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            news: [],
            page: 0,
            numberPerPage: 10,
            totalEntityNumber: -1,
            message: null
        }
        this.refreshNews = this.refreshNews.bind(this)
        this.addNewsClicked = this.addNewsClicked.bind(this)
    }

    componentDidMount() {
        this.refreshNews(this.state.page);
    }

    refreshNews(currentPage, newNumberPerPage) {
        if (!newNumberPerPage) {
            newNumberPerPage = this.state.numberPerPage;
        }
        NewsDataService.getAll(currentPage, newNumberPerPage)
            .then(
                response => {
                    let newTotalEntityNumber = Number.parseInt(response.headers.totalentitycount);
                    let pageQuantity = Math.ceil(newTotalEntityNumber / newNumberPerPage);
                    if (currentPage >= pageQuantity) {
                        currentPage = pageQuantity - 1;
                    }
                    this.setState({
                        news: response.data,
                        totalEntityNumber: newTotalEntityNumber,
                        page: currentPage,
                        numberPerPage: newNumberPerPage
                    });
                }
            )
    }

    deleteNewsClicked(id) {
        NewsDataService.delete(id)
            .then(
                response => {
                    this.setState({ message: this.props.t("list.newsDeletedMessage", {deletedId: id}) })
                    this.refreshNews()
                }
            )
    }

    updateNewsClicked(id) {
        this.props.history.push("/news/" + id);
    }

    addNewsClicked() {
        this.props.history.push(`/news/-1`)
    }

    render() {
        const newsRender = this.state.news.map(singleNews => {
            return (
                <tr key = {singleNews.id}>
                    <td>{singleNews.id}</td>
                    <td>{singleNews.title}</td>
                    <td>{singleNews.shortText}</td>
                    <td>{singleNews.creationDate}</td>
                    <td>{singleNews.modificationDate}</td>
                    <td>{singleNews.authorDto.name + " " + singleNews.authorDto.surname}</td>
                    <td>{singleNews.tagDtoSet.map(tag => {
                        return (tag.name + " ")
                    })}</td>
                    <td>
                        <button className="btn btn-warning" 
                                onClick={() => this.deleteNewsClicked(singleNews.id)}
                                >{this.props.t("list.deleteBtn")}
                        </button>
                    </td>
                    <td>
                        <button className="btn btn-success" 
                                onClick={() => this.updateNewsClicked(singleNews.id)}
                                >{this.props.t("list.updateBtn")}
                        </button>
                    </td>
                </tr>
            )
        });
        let sidebarButtons = [
            {
                title: this.props.t("news.add"),
                class: "btn btn-success",
                onClick: () => {window.location.href = "/news/-1"}
            }
        ]
        return (
            <div className="row">
                <div className="col-2">
                    <Sidebar buttons={sidebarButtons}/>
                </div>
                <div className="col-8">
                    <h3 className="ml-4 my-3">{this.props.t("list.newsTitle")}</h3>
                    {this.state.message && <div class="alert alert-success">{this.state.message}</div>}
                    <table className="table">
                        <thead>
                            <tr>
                                <th>{this.props.t("news.id")}</th>
                                <th>{this.props.t("news.title")}</th>
                                <th>{this.props.t("news.shortText")}</th>
                                <th>{this.props.t("news.creationDate")}</th>
                                <th>{this.props.t("news.modificationDate")}</th>
                                <th>{this.props.t("news.author")}</th>
                                <th>{this.props.t("news.tags")}</th>
                                <th colSpan="2">{this.props.t("list.actions")}</th>
                            </tr>
                        </thead>
                        <tbody>
                            {newsRender}
                        </tbody>
                    </table>
                    <Pagination currentPage={this.state.page} totalEntityNumber={this.state.totalEntityNumber} numberPerPage={this.state.numberPerPage} changePage={this.refreshNews}/>
                </div>
                <div className="col-2"></div>
            </div>
        )
    }
}

export default withTranslation()(ListNewsComponent)