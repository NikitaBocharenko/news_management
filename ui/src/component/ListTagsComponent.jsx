import React from 'react';
import TagsDataService from '../service/TagsDataService';
import Pagination from './Pagination';
import Sidebar from './Sidebar';
import { withTranslation } from 'react-i18next';

class ListTagsComponent extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            tags: [],
            page: 0,
            numberPerPage: 10,
            totalEntityNumber: -1,
            message: null
        }
        this.addTagClicked = this.addTagClicked.bind(this)
        this.refreshTags = this.refreshTags.bind(this)
    }

    componentDidMount() {
        this.refreshTags(this.state.page);
    }

    refreshTags(currentPage, newNumberPerPage) {
        if (!newNumberPerPage) {
            newNumberPerPage = this.state.numberPerPage;
        }
        TagsDataService.getAll(currentPage, newNumberPerPage)
            .then(
                response => {
                    let newTotalEntityNumber = Number.parseInt(response.headers.totalentitycount);
                    let pageQuantity = Math.ceil(newTotalEntityNumber / newNumberPerPage);
                    if (currentPage >= pageQuantity) {
                        currentPage = pageQuantity - 1;
                    }
                    this.setState({
                        tags: response.data,
                        totalEntityNumber: newTotalEntityNumber,
                        page: currentPage,
                        numberPerPage: newNumberPerPage
                    });
                }
            )
    }

    deleteTagClicked(id) {
        TagsDataService.delete(id)
            .then(
                response => {
                    this.setState({ message: this.props.t("list.tagDeletedMessage", {deletedId: id}) })
                    this.refreshTags()
                }
            )
    }

    updateTagClicked(id) {
        this.props.history.push("/tags/" + id);
    }

    addTagClicked() {
        this.props.history.push(`/tags/-1`)
    }

    render() {
        const tagsRender = this.state.tags.map(tag => {
            return (
                <tr key = {tag.id}>
                    <td>{tag.id}</td>
                    <td>{tag.name}</td>
                    <td>
                        <button className="btn btn-warning" 
                                onClick={() => this.deleteTagClicked(tag.id)}
                                >{this.props.t("list.deleteBtn")}
                        </button>
                    </td>
                    <td>
                        <button className="btn btn-success" 
                                onClick={() => this.updateTagClicked(tag.id)}
                                >{this.props.t("list.updateBtn")}
                        </button>
                    </td>
                </tr>
            )
        });
        let sidebarButtons = [
            {
                title: this.props.t("tag.add"),
                class: "btn btn-success",
                onClick: () => {window.location.href = "/tags/-1"}
            }
        ];
        return (
            <div className="row">
                <div className="col-2">
                    <Sidebar buttons={sidebarButtons}/>
                </div>
                <div className="col-8">
                    <h3 className="ml-4 my-3">{this.props.t("list.tagTitle")}</h3>
                    {this.state.message && <div class="alert alert-success">{this.state.message}</div>}
                    <table className="table">
                        <thead>
                            <tr>
                                <th>{this.props.t("tag.id")}</th>
                                <th>{this.props.t("tag.name")}</th>
                                <th colSpan="2">{this.props.t("list.actions")}</th>
                            </tr>
                        </thead>
                        <tbody>
                            {tagsRender}
                        </tbody>
                    </table>
                    <Pagination currentPage={this.state.page} totalEntityNumber={this.state.totalEntityNumber} numberPerPage={this.state.numberPerPage} changePage={this.refreshTags}/>
                </div>
                <div className="col-2"></div>
            </div>
        )
    }
}

export default withTranslation()(ListTagsComponent)