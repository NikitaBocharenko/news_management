import React from 'react'
import UserDataService from '../service/UserDataService'
import LanguageSwitcher from '../service/LanguageSwitcher'
import { Formik, Field, Form, ErrorMessage} from 'formik'
import { withTranslation } from 'react-i18next';

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.onSubmit = this.onSubmit.bind(this)
        this.validate = this.validate.bind(this)
    }

    onSubmit(values) {
        console.log("ON SUBMIT");
        console.log(values);
        const user = {
            login: values.login,
            password: values.password
        }
        UserDataService.logIn(user)
            .then(
                response => {
                    console.log(response.data);
                    if (response.data) {
                        sessionStorage.setItem("userName", response.data.name + " " + response.data.surname);
                        window.location.reload();
                    } else {
                        let errorDiv = document.createElement('div');
                        errorDiv.id = "loginErrorMessage";
                        errorDiv.className = "alert alert-warning";
                        errorDiv.innerText = this.props.t("login.wrongCredentials");
                        let title = document.getElementById("title");
                        title.after(errorDiv);
                    }
                }
            )
    }

    validate(values) {
        console.log("VALIDATE");
        console.log(values);
        let previousLoginErrorMessage = document.getElementById("loginErrorMessage");
        if (previousLoginErrorMessage) {
            previousLoginErrorMessage.remove();
        }
        let errors = {};
        if (!values.login) {
            errors.login = this.props.t("validation.user.login.empty");
        } else if (values.login.length > 30) {
            errors.login = this.props.t("validation.user.login.overlong");
        }
        if (!values.password) {
            errors.password = this.props.t("validation.user.password.empty");
        } else if (values.password.length > 30) {
            errors.password = this.props.t("validation.user.password.overlong");
        }
        return errors;
    }

    render() {
        let login = "";
        let password = "";
        let user = {
            grant_type: "password",
            username: "slav",
            password: "grad"
        };
        UserDataService.getToken(user)
            .then(
                response => {
                    console.log(response.data);
                }
            );
        return (
            <div className="container">
                <div style={{height: "200px"}}></div>
                <div className="row">
                    <div className="col-5">
                        <h3 id="title">{this.props.t("login.title")}</h3>
                        <Formik initialValues={{ login, password }} 
                                onSubmit={this.onSubmit}
                                validateOnChange={false}
                                validateOnBlur={false}
                                validate={this.validate}>
                            {(props) => (
                                <Form>
                                    <fieldset className="form-group">
                                        <label>{this.props.t("login.user")}</label>
                                        <Field className="form-control" type="text" name="login" />
                                    </fieldset>
                                    <ErrorMessage name="login" component="div"
                                        className="alert alert-warning" />
                                    <fieldset className="form-group">
                                        <label>{this.props.t("login.password")}</label>
                                        <Field className="form-control" type="password" name="password" />
                                    </fieldset>
                                    <ErrorMessage name="password" component="div"
                                        className="alert alert-warning" />
                                    <div className="row">
                                        <div className="col-3">
                                            <button className="btn btn-success" type="submit">{this.props.t("login.submit")}</button>
                                        </div>
                                        <div className="col-5">
                                        </div>
                                        <div className="col-3">
                                            <div className="dropdown">
                                                <button className="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    {this.props.t("language.title")}
                                                </button>
                                                <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                    <a className="dropdown-item" href="#" onClick={()=>LanguageSwitcher.changeLanguage("en")}>{this.props.t("language.en")}</a>
                                                    <a className="dropdown-item" href="#" onClick={()=>LanguageSwitcher.changeLanguage("ru")}>{this.props.t("language.ru")}</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </Form>
                            )
                        }</Formik>
                    </div>
                </div>
            </div>
        );
    }
}

export default withTranslation()(Login)