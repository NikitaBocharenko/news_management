import React from 'react'
import { Field, ErrorMessage } from 'formik'
import Select from 'react-select'
import AuthorDataService from '../service/AuthorDataService'
import { withTranslation } from 'react-i18next';

class NewsAuthorComponent extends React.Component {
    constructor(props) {
        console.log("CONSTRUCTOR AUTHOR");
        super(props);
        this.state = {
            allAuthors: [],
            selectedAuthor: props.author
        }
        this.onSelectOptionClicked = this.onSelectOptionClicked.bind(this);
    }

    componentDidMount() {
        AuthorDataService.getAll(0,0).then(
            response => {
                console.log("COMPONENT DID MOUNT AUTHOR");
                this.setState({allAuthors: response.data});
            }
        );
    }

    onSelectOptionClicked(event) {
        let newSelectedAuthor = {
            id: event.value,
            name: event.label.split(" ")[0],
            surname: event.label.split(" ")[1],
        };
        this.setState({selectedAuthor: newSelectedAuthor});
    }

    render() {
        console.log("RENDER AUTHOR");
        const authorOptions = [];
        for (let author of this.state.allAuthors) {
            authorOptions.push({
                label: author.name + " " + author.surname,
                value: author.id,
                key: author.id
            });
        };
        
        let authorSelected;
        if (Object.keys(this.state.selectedAuthor).length !== 0) {
            authorSelected = {
                label: this.state.selectedAuthor.name + " " + this.state.selectedAuthor.surname,
                value: this.state.selectedAuthor.id,
            };
        }

        return(
            <div class="col">
                <h4>{this.props.t("news.author")}</h4>
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">
                            {this.props.t("newsAuthor.existingAuthor")}
                        </a>
                        <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">
                            {this.props.t("newsAuthor.newAuthor")}
                        </a>
                    </div>
                </nav>
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                        <fieldset className="form-group" id="authorSelectFieldSet">
                            <label>{this.props.t("newsAuthor.selectAuthor")}</label>
                            <Select name="authorSelect" value={authorSelected} 
                                options={authorOptions} onChange={this.onSelectOptionClicked} />
                        </fieldset>
                    </div>
                    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                        <fieldset className="form-group">
                            <label>{this.props.t("author.name")}</label>
                            <Field className="form-control" type="text" name="authorName" />
                        </fieldset>
                        <ErrorMessage name="authorName" component="div"
                            className="alert alert-warning" />
                        <fieldset className="form-group">
                            <label>{this.props.t("author.surname")}</label>
                            <Field className="form-control" type="text" name="authorSurname" />
                        </fieldset>
                        <ErrorMessage name="authorSurname" component="div"
                            className="alert alert-warning" />
                    </div>
                </div>
            </div>
        )
    }
}

export default withTranslation()(NewsAuthorComponent)