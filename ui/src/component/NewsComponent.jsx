import React from 'react'
import NewsDataService from '../service/NewsDataService'
import NewsAuthorComponent from './NewsAuthorComponent'
import NewsTagsComponent from './NewsTagsComponent'
import { Formik, Field, Form, ErrorMessage} from 'formik'
import { withTranslation } from 'react-i18next';
import Sidebar from './Sidebar';

class NewsComponent extends React.Component {
    constructor(props) {
        console.log("CONSTRUCTOR NEWS");
        super(props)
        this.state = {
            id: this.props.match.params.id,
            title: "",
            shortText: "",
            fullText: "",
            creationDate: "",
            modificationDate: "",
            authorDto: {},
            tagDtoSet: [],
        }
        this.onSubmit = this.onSubmit.bind(this)
        this.validate = this.validate.bind(this)
    }

    componentDidMount() {
        if (this.state.id == -1) {
            return;
        }
        NewsDataService.getById(this.state.id)
            .then(
                response => {
                    console.log("COMPONENT DID MOUNT NEWS");
                    this.setState({
                        title: response.data.title,
                        shortText: response.data.shortText,
                        fullText: response.data.fullText,
                        creationDate: response.data.creationDate,
                        modificationDate: response.data.modificationDate,
                        authorDto: response.data.authorDto,
                        tagDtoSet: response.data.tagDtoSet
                    });
            });
    }

    getCurrentDate() {
        let today = new Date();
        let month = String(today.getMonth() + 1).padStart(2, '0');
        return today.getFullYear() + '-' + month + '-' + today.getDate();
    }

    buildAuthor(values) {
        let authorDto;
        if (values.authorName) {
            authorDto = {
                name: values.authorName,
                surname: values.authorSurname
            }
        } else {
            let authorSelect = document.getElementsByName("authorSelect")[0];
            authorDto = {
                id: authorSelect.value,
            }
        }
        return authorDto;
    }

    buildTags() {
        let tagDtoSet = [];
        let tagsInputs = document.getElementsByName("tagsSelect");
        for (let input of tagsInputs) {
            if (input.value) {
                let numberValue = Number.parseInt(input.value);
                if (numberValue) {
                    tagDtoSet.push({
                        id: numberValue,
                    });
                } else {
                    tagDtoSet.push({
                        name: input.value,
                    });
                }
            }
        }
        return tagDtoSet;
    }

    onSubmit(values) {
        let authorDto = this.buildAuthor(values);
        let tagDtoSet = this.buildTags();
        let news = {
            title: values.title,
            shortText: values.shortText,
            fullText: values.fullText,
            modificationDate: this.getCurrentDate(),
            authorDto: authorDto,
            tagDtoSet: tagDtoSet
        }
        if (this.state.id == -1) {
            news.creationDate = this.getCurrentDate();
            console.log(news);
            NewsDataService.add(news)
                .then(() => this.props.history.push("/news"));
        } else {
            news.id = this.state.id;
            news.creationDate = this.state.creationDate;
            console.log(news);
            NewsDataService.edit(news)
                .then(() => this.props.history.push("/news"));
        }
    }

    validateAuthor(values, errors) {
        let previousAuthorErrorMessage = document.getElementById("authorSelectErrorMessage");
        if (previousAuthorErrorMessage) {
            previousAuthorErrorMessage.remove();
        }
        if (!values.authorName && !values.authorSurname) {
            let authorSelect = document.getElementsByName("authorSelect")[0];
            if (!authorSelect.value) {
                let div = document.createElement('div');
                div.id = "authorSelectErrorMessage";
                div.className = "alert alert-warning";
                div.innerText = this.props.t("validation.news.authorNotSelected");
                let fieldSet = document.getElementById("authorSelectFieldSet");
                fieldSet.append(div);
                errors.authorSelect = this.props.t("validation.news.authorNotSelected");
            }
        } else if (values.authorName && !values.authorSurname) {
            errors.authorSurname = this.props.t("validation.author.surname.empty");
        } else if (!values.authorName && values.authorSurname) {
            errors.authorName = this.props.t("validation.author.name.empty");
        } else if (values.authorName.length > 30) {
            errors.authorName = this.props.t("validation.author.name.overlong");
        } else if (values.authorSurname.length > 30) {
            errors.authorSurname = this.props.t("validation.author.surname.overlong");
        }
    }

    validate(values) {
        let errors = {}
        if (!values.title) {
            errors.title = this.props.t("validation.news.title.empty");
        } else if (values.title.length > 30) {
            errors.title = this.props.t("validation.news.title.overlong");
        }
        if (!values.shortText) {
            errors.shortText = this.props.t("validation.news.shortText.empty");
        } else if (values.shortText.length > 100) {
            errors.shortText = this.props.t("validation.news.shortText.overlong");
        }
        if (!values.fullText) {
            errors.fullText = this.props.t("validation.news.fullText.empty");
        } else if (values.fullText.length > 2000) {
            errors.fullText = this.props.t("validation.news.fullText.empty");
        }
        this.validateAuthor(values, errors);
        console.log(errors);
        return errors
    }

    render() {
        if (this.state.id != -1 && !this.state.creationDate) {
            return null;
        }
        console.log("RENDER NEWS");
        let { 
            title, 
            shortText, 
            fullText,
            authorDto,
            tagDtoSet
        } = this.state;
        let authorName = "";
        let authorSurname = "";
        let sidebarButtons = [
            {
                title: this.props.t("component.saveBtn"),
                class: "btn btn-success",
                type: "submit",
                form: "newsForm"
            },
            {
                title: this.props.t("component.resetBtn"),
                class: "btn btn-warning",
                type: "reset",
                form: "newsForm"
            },
            {
                title: this.props.t("component.cancel"),
                class: "btn btn-danger",
                onClick: () => {window.history.back()}
            }
        ]
        return (
            <div className="row">
                <div className="col-2">
                    <Sidebar buttons={sidebarButtons}/>
                </div>
                <div className="col-8">
                    <h3 className="ml-4 my-3">{this.props.t("news.entityName")}</h3>
                    <Formik initialValues={{ title, shortText, fullText, authorName, authorSurname}} 
                            onSubmit={this.onSubmit}
                            validateOnChange={false}
                            validateOnBlur={false}
                            validate={this.validate} 
                            enableReinitialize={true}>
                    {(props) => (
                            <Form id="newsForm">
                                <fieldset className="form-group">
                                    <label>{this.props.t("news.title")}</label>
                                    <Field className="form-control" type="text" name="title" />
                                </fieldset>
                                <ErrorMessage name="title" component="div"
                                    className="alert alert-warning" />
                                <fieldset className="form-group">
                                    <label>{this.props.t("news.shortText")}</label>
                                    <Field className="form-control" type="text" name="shortText" />
                                </fieldset>
                                <ErrorMessage name="shortText" component="div"
                                    className="alert alert-warning" />
                                <fieldset className="form-group">
                                    <label>{this.props.t("news.fullText")}</label>
                                    <Field className="form-control" component="textarea" rows="10" name="fullText" />
                                </fieldset>
                                <ErrorMessage name="fullText" component="div"
                                    className="alert alert-warning" />
                                <div class="row">
                                    <NewsAuthorComponent author={authorDto} />
                                    <NewsTagsComponent tags={tagDtoSet} />
                                </div>
                            </Form>
                        )
                    }</Formik>
                </div>
                <div className="col-2"></div>
            </div>
        )
    }
}

export default withTranslation()(NewsComponent)