import React from 'react'
import TagsDataService from '../service/TagsDataService'
import Select from 'react-select'
import { withTranslation } from 'react-i18next';

class NewsTagsComponent extends React.Component {
    constructor(props) {
        console.log("CONSTRUCTOR TAGS");
        super(props);
        this.state = {
            selectedTags: props.tags,
            allTags: []
        }
        this.onSelectOptionClicked = this.onSelectOptionClicked.bind(this);
        this.onAddTagButtonClicked = this.onAddTagButtonClicked.bind(this);
    }

    componentDidMount() {
        TagsDataService.getAll(0,0).then(
            response => {
                console.log("COMPONENT DID MOUNT TAGS");
                this.setState({allTags: response.data});
            }
        )
    }

    onSelectOptionClicked(selectedTagOptions) {
        console.log("SELECT OPTION CLICKED");
        if (selectedTagOptions) {
            const newSelectedTags = [];
            for (let tagOption of selectedTagOptions) {
                newSelectedTags.push({
                    id: tagOption.value,
                    name: tagOption.label
                });
            }
            this.setState({selectedTags: newSelectedTags});
        } else {
            this.setState({selectedTags: []});
        }
    }

    validateNewsTagValue(value) {
        let errorMessage = "";
        if (!value) {
            errorMessage = this.props.t("validation.tag.name.empty");
        } else if (Number.parseInt(value)) {
            errorMessage = this.props.t("validation.tag.name.notChars");
        } else if (value.length > 30) {
            errorMessage = this.props.t("validation.tag.name.overlong");
        } else {
            const allTags = this.state.allTags;
            for (let tag of allTags) {
                if (tag.name === value) {
                    errorMessage = this.props.t("validation.tag.name.nameExists");
                    break;
                }
            }
        }
        return errorMessage;
    }

    onAddTagButtonClicked() {
        let previousErrorMessage = document.getElementById("tagNameErrorMessage");
        if (previousErrorMessage) {
            previousErrorMessage.remove();
        }
        let newTagInput = document.getElementById("newTag");
        let errorMessage = this.validateNewsTagValue(newTagInput.value);
        if (errorMessage) {
            let div = document.createElement('div');
            div.id = "tagNameErrorMessage";
            div.className = "alert alert-warning";
            div.innerText = errorMessage;
            let fieldSet = document.getElementById("newTagFieldSet");
            fieldSet.after(div);
            return;
        }
        let newSelectedTags = this.state.selectedTags;
        newSelectedTags.push({
            id: newTagInput.value,
            name: newTagInput.value,
        });
        newTagInput.value = "";
        this.setState({selectedTags: newSelectedTags});
    }

    render() {
        console.log("RENDER TAGS");

        const tagsOptions = [];
        for (let tag of this.state.allTags) {
            tagsOptions.push({
                label: tag.name,
                value: tag.id,
                key: tag.id
            });
        };
        
        const tagsSelected = [];
        for (let tag of this.state.selectedTags) {
            tagsSelected.push({
                label: tag.name,
                value: tag.id,
                key: tag.id
            });
        };
        
        return(
            <div class="col">
                <h4>{this.props.t("news.tags")}</h4>
                <fieldset>
                    <Select name="tagsSelect" value={tagsSelected} 
                        options={tagsOptions} onChange={this.onSelectOptionClicked} 
                        isMulti />
                </fieldset>
                <fieldset className="form-group" id="newTagFieldSet">
                    <label>{this.props.t("newsTags.newTag")}</label>
                    <input className="form-control" type="text" id="newTag" name="newTag" />
                </fieldset>
                <button className="btn btn-success" type="button" onClick={this.onAddTagButtonClicked}>{this.props.t("newsTags.addTagBtn")}</button>
            </div>  
        )
    }
}

export default withTranslation()(NewsTagsComponent)