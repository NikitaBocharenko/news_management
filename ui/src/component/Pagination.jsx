import React from 'react'
import { withTranslation } from 'react-i18next';

class Pagination extends React.Component {
    getLiClass(type, pageQuantity) {
        if (type === "first" && this.props.currentPage === 0 || type === "last" && this.props.currentPage === pageQuantity - 1) {
            return "page-item disabled";
        } else {
            return "page-item";
        }
    }

    render() {
        let pageQuantity = Math.ceil(this.props.totalEntityNumber / this.props.numberPerPage);
        const renderPaginationLinks = () => {
            let paginationLinks = [];
            for (let i = 0; i < pageQuantity; i++) {
                let linkClass = i === this.props.currentPage ? "page-item active" : "page-item";
                paginationLinks.push(<li key = {i} className={linkClass}><a className="page-link" href="#" onClick={() => this.props.changePage(i)}>{i+1}</a></li>);
            }
            return (paginationLinks);
        }
        return(
            <div className="row">
                <div className="col"></div>
                <div className="col">
                    <nav aria-label="Page navigation example">
                        <ul className="pagination justify-content-center">
                            <li className={this.getLiClass("first", pageQuantity)}>
                                <a className="page-link" href="#" onClick={() => this.props.changePage(this.props.currentPage - 1)}>{this.props.t("pagination.prev")}</a>
                            </li>
                            {renderPaginationLinks()}
                            <li className={this.getLiClass("last", pageQuantity)}>
                                <a className="page-link" href="#" onClick={() => this.props.changePage(this.props.currentPage + 1)}>{this.props.t("pagination.next")}</a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div className="col">
                    <div className="btn-group">
                        <button type="button" className="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {this.props.t("pagination.resultsNumber")}: {this.props.numberPerPage}
                        </button>
                        <div className="dropdown-menu">
                            <a className="dropdown-item" href="#" onClick={() => this.props.changePage(this.props.currentPage, 10)}>10</a>
                            <a className="dropdown-item" href="#" onClick={() => this.props.changePage(this.props.currentPage, 20)}>20</a>
                            <a className="dropdown-item" href="#" onClick={() => this.props.changePage(this.props.currentPage, 50)}>50</a>
                        </div>
                    </div>
                </div>
            </div>
            
        )
    }
}

export default withTranslation()(Pagination)