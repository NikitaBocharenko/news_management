import React from 'react';

class Sidebar extends React.Component {
    render() {
        const buttonsRender = this.props.buttons.map(button => {
            if (button.type) {
                return (
                    <button type="button" className={"list-group-item list-group-item-action my-1 " + button.class} type={button.type} form={button.form}>{button.title}</button>
                )
            }
            return (
                <button type="button" className={"list-group-item list-group-item-action my-1 " + button.class} onClick={button.onClick}>{button.title}</button>
            )
        });
        return (
            <div style={{width: "15%", top: "13%"}} className="position-fixed bg-light ml-2">
                <div className="list-group mx-2 my-2">
                    {buttonsRender}
                </div>
            </div>
            
        )
    }
}

export default Sidebar