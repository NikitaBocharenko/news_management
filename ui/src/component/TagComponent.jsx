import React from 'react'
import TagsDataService from '../service/TagsDataService'
import { Formik, Field, Form, ErrorMessage} from 'formik'
import { withTranslation } from 'react-i18next';
import Sidebar from './Sidebar';

class TagComponent extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            id: this.props.match.params.id,
            name: ""
        }
        this.onSubmit = this.onSubmit.bind(this)
        this.validate = this.validate.bind(this)
    }

    componentDidMount() {
        console.log(this.state.id)
        if (this.state.id == -1) {
            return;
        }
        TagsDataService.getById(this.state.id)
            .then(
                response => {
                    console.log(response.data);
                    this.setState({name: response.data.name});
            });
    }

    onSubmit(values) {
        console.log(values);
        let tag = {
            name: values.name
        }
        if (this.state.id == -1) {
            TagsDataService.add(tag)
                .then(() => this.props.history.push("/tags"));
        } else {
            tag.id = this.state.id;
            TagsDataService.edit(tag)
                .then(() => this.props.history.push("/tags"));
        }
    }

    validate(values) {
        let errors = {}
        if (!values.name) {
            errors.name = this.props.t("validation.tag.name.empty")
        } else if (values.name.length > 30) {
            errors.name = this.props.t("validation.tag.name.overlong")
        }
        return errors
    }

    render() {
        let name = this.state.name;
        let sidebarButtons = [
            {
                title: this.props.t("component.saveBtn"),
                class: "btn btn-success",
                type: "submit",
                form: "tagForm"
            },
            {
                title: this.props.t("component.resetBtn"),
                class: "btn btn-warning",
                type: "reset",
                form: "tagForm"
            },
            {
                title: this.props.t("component.cancel"),
                class: "btn btn-danger",
                onClick: () => {window.history.back()}
            }
        ];
        return (
            <div className="row">
                <div className="col-2">
                    <Sidebar buttons={sidebarButtons}/>
                </div>
                <div className="col-8">
                    <h3 className="ml-4 my-3">{this.props.t("tag.entityName")}</h3>
                    <Formik initialValues={{ name }} 
                            onSubmit={this.onSubmit}
                            validateOnChange={false}
                            validateOnBlur={false}
                            validate={this.validate} 
                            enableReinitialize={true}>
                    {(props) => (
                            <Form id="tagForm">
                                <fieldset className="form-group">
                                    <label>{this.props.t("tag.name")}</label>
                                    <Field className="form-control" type="text" name="name" />
                                </fieldset>
                                <ErrorMessage name="name" component="div"
                                    className="alert alert-warning" />
                                <button className="btn btn-success" type="submit">{this.props.t("component.saveBtn")}</button>
                            </Form>
                        )
                    }</Formik>
                </div>
            </div>
        )
    }
}

export default withTranslation()(TagComponent)