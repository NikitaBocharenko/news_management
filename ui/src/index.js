import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap-select/dist/css/bootstrap-select.css'
import 'bootstrap/dist/js/bootstrap.min.js'
import 'bootstrap-select/dist/js/bootstrap-select.min.js'
import {I18nextProvider} from 'react-i18next';
import i18next from 'i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import translation_EN from './locales/en/translation.json';
import translation_RU from './locales/ru/translation.json';
import App from './App';

const options = {
    order: [ 'cookie' ],
    lookupCookie: 'lng',
};

i18next
    .use(LanguageDetector)
    .init({
        interpolation: { escapeValue: false },
        detection: options,       
        fallbackLng: "en",                       
        defaultNS: 'default',
        resources: {
            en: {
                default: translation_EN               
            },
            ru: {
                default: translation_RU
            },
        },
    });

ReactDOM.render(
    <I18nextProvider i18n={i18next}>
        <App/>
    </I18nextProvider>, 
    document.getElementById('root'));