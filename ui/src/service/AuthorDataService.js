import axios from 'axios'

const AUTHOR_API_URL = 'http://localhost:8081/news_management/authors'

class AuthorDataService {
    getAll(page, numberPerPage) {
        return axios.get(AUTHOR_API_URL + "?page=" + page + "&numberPerPage=" + numberPerPage);
    }

    delete(id) {
        return axios.delete(AUTHOR_API_URL + "/" + id);
    }

    getById(id) {
        return axios.get(AUTHOR_API_URL + "/" + id);
    }

    edit(author) {
        return axios.put(AUTHOR_API_URL, author);
    }
  
    add(author) {
        return axios.post(AUTHOR_API_URL, author);
    }
}

export default new AuthorDataService()