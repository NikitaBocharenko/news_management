class LanguageSwitcher {
    changeLanguage(lng) {
        document.cookie = "lng=" + lng;
        window.location.reload();
    }
}

export default new LanguageSwitcher()