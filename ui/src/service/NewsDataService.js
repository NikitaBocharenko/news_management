import axios from 'axios'

const NEWS_API_URL = 'http://localhost:8081/news_management/news'

class NewsDataService {
    getAll(page, numberPerPage) {
        return axios.get(NEWS_API_URL + "?page=" + page + "&numberPerPage=" + numberPerPage);
    }

    delete(id) {
        return axios.delete(NEWS_API_URL + "/" + id);
    }

    getById(id) {
        return axios.get(NEWS_API_URL + "/" + id);
    }

    edit(news) {
        return axios.put(NEWS_API_URL, news);
    }
  
    add(news) {
        return axios.post(NEWS_API_URL, news);
    }
}

export default new NewsDataService()