import axios from 'axios'

const TAG_API_URL = 'http://localhost:8081/news_management/tags'

class TagsDataService {
    getAll(page, numberPerPage) {
        return axios.get(TAG_API_URL + "?page=" + page + "&numberPerPage=" + numberPerPage);
    }

    delete(id) {
        return axios.delete(TAG_API_URL + "/" + id);
    }

    getById(id) {
        return axios.get(TAG_API_URL + "/" + id);
    }

    edit(tag) {
        return axios.put(TAG_API_URL, tag);
    }
  
    add(tag) {
        return axios.post(TAG_API_URL, tag);
    }
}

export default new TagsDataService()