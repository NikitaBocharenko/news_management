import axios from 'axios'

const USER_API_URL = 'http://localhost:8081/news_management/users'

class UserDataService {
    logIn(user) {
        return axios.post(USER_API_URL + "/login", user);
    }

    getToken(user) {
        return axios.post('http://localhost:8081/news_management/oauth/token', user);
    }
}

export default new UserDataService()